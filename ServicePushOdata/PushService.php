<?php

use Bitrix\Main\EventManager;
//use Bitrix\Rest\Api\Events;
use Bitrix\Rest\EventTable;

if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}

if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushDealRequest.php');
}

if (!class_exists('PushFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushFormatterInterface.php');
}
if (!class_exists('PushDealFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushDealFormatter.php');
}
if (!class_exists('PushCompanyFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushCompanyFormatter.php');
}
if (!class_exists('PushPartnersFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushPartnersFormatter.php');
}

if (!class_exists('PushBanksRequisiteFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushBanksRequisiteFormatter.php');
}
if (!class_exists('Events')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Events/Events.php');
}
if (!class_exists('Deal')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Deal.php');
}
if (!class_exists('EntityInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/EntityInterface.php');
}
if (!class_exists('BanksCatalog')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/OdataEntity/BanksCatalog.php');
}
if (!class_exists('BanksRequisite')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/BanksRequisite.php');
}
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');

class PushService
{


    private $requisite = [];
    private $entity;
    private $entitys;
    private $pushFormatter;
    protected $client;
    private $bitrixclient;
    private $events;
    private $onwerKey;
   private $workflow;

    /**
     * PushService constructor.
     * @param PushFormatterInterface $pushFormatter
     * @throws Exception
     *
     */
    public function __construct(PushFormatterInterface $pushFormatter, EntityInterface $entity)
    {
        $this->bitrixclient = new \ApiBitirxClient();
        $this->pushFormatter = $pushFormatter;
        $this->client = new \ApiErpOdataClient();
        $this->events = new \Events();
        $this->onwerKey= new PushDealRequest();
        $this->workflow=new WorkflowBizprocService();
        $this->entity = $entity;
    }


    /**
     *
     */
    public function event($name)
    {

        foreach ($this->events->pushEvents() as $event) {
            if($this->events->entityEvent($event['EVENT_NAME'])===$name)
            $this->pushOdata($event);
            $this->workflow->is_Status($event);

        }

    }

    /**
     * @param $id
     */
    public function pushOdata($events)
    {
       $guid=null;

        if ($this->pushFormatter->setOnwerKey($events['CONSEGNEE'] ? $events['CONSEGNEE'] : '')) {

        }
        $guid =$this->entity->getReturnGuid($events['ENTITY_ID']);

            $response = $this->client->RestPushApiClient(
            $this->client->convertURL(
                $events['URL'],
                $guid),
            $this->pushFormatter->Format($this->entity->getEntity($events['ENTITY_ID'])),
                $guid ? 'PATCH' : 'POST');

        if (!array_key_exists('odata.error', $response)) {
            $this->entity->setReturnGuid($response['Ref_Key'], $events['ENTITY_ID']);
            $this->events->EventsListsClear($events['MESSAGE_ID']);
            echo 'Успешно';
        } else {

            if ($response['odata.error']['code']) {

            //    $bitrixclient = new ApiBitirxClient();
            //    $bitrixclient->ApiClient(['POST_TITLE'=>'Error','POST_MESSAGE'=>json_encode( $response,JSON_UNESCAPED_UNICODE),'DEST'=>['U92']], 'log.blogpost.add');
            }

        }

     //   $bitrixclient = new ApiBitirxClient();
    //    $bitrixclient->ApiClient(['POST_TITLE'=>'pushData','POST_MESSAGE'=>  $this->client->convertURL(
    //        $events['URL'],
    //            $this->entity->getReturnGuid($events['ENTITY_ID'])).'--+++---'.$this->entity->getReturnGuid($events['ENTITY_ID']).'----'.($this->entity->getReturnGuid($events['ENTITY_ID']) ? 'PATCH' : 'POST').'----'.json_encode($events,JSON_UNESCAPED_UNICODE).'----'.json_encode($response,JSON_UNESCAPED_UNICODE).'****'.json_encode($this->pushFormatter->Format($this->entity->getEntity($events['ENTITY_ID'])),JSON_UNESCAPED_UNICODE).'*********','DEST'=>['U92']], 'log.blogpost.add');


        return $response;
    }

    public function test()
    {
       // print_r( $this->pushFormatter->Format($this->entity->getEntity(1244)));
//           $bank= new  BanksCatalog();
//          print_r($bank->getFetchGuid('040495000'));
       $bitrixclient = new ApiBitirxClient();
     //   print_r(json_decode( $bitrixclient->ApiClient(['id'=>20162], 'crm.requisite.get'),true));
     print_r(json_decode($bitrixclient->ApiClient(['id'=>1244], 'crm.requisite.bankdetail.get'),true));
    }


    public function CtreateEvents(){
   //print_r(json_decode($this->bitrixclient->ApiClient(["auth" => $_REQUEST['AUTH_ID'],'auth_type'=>0,'event'=>'onCrmCompanyAdd','handler'=>'https://10.97.21.13/','event_type'=>'offline'], 'event.bind'),true));
 // print_r(json_decode($this->bitrixclient->ApiClient(["auth" => $_REQUEST['AUTH_ID'],'auth_type'=>0,'event'=>'onCrmCompanyUpdate','handler'=>'https://10.97.21.13','event_type'=>'offline'], 'event.bind'),true));
    //print_r(json_decode($this->bitrixclient->ApiClient(["auth" => $_REQUEST['AUTH_ID'],'auth_type'=>0,'event'=>'onCrmBankDetailAdd','handler'=>'https://10.97.21.13/','event_type'=>'offline'], 'event.bind'),true));
  //print_r(json_decode($this->bitrixclient->ApiClient(["auth" => $_REQUEST['AUTH_ID'],'auth_type'=>0,'event'=>'onCrmBankDetailUpdate','handler'=>'https://10.97.21.13','event_type'=>'offline'], 'event.bind'),true));

   // print_r(json_decode($this->bitrixclient->ApiClient(["auth" => $_REQUEST['AUTH_ID']], 'event.offline.get'),true));

    }



}