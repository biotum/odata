<?php

if (!class_exists('OdataCfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/Odatacfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ApiErp/ApiErpOdataClient.php');
}

if (!class_exists('FormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
}



class BasePushOdataService
{

    protected $featchrequest;
    protected $valuesearch;
    protected $featchClientsrequest;
    protected $listUpdate = [];
    protected $listClientsUpdate = [];
    protected $listRequisiteUpdate = [];
    protected $listClients = [];
    protected $listRequisite = [];
    protected $listAddressRequisite = [];
    protected $Clientslist=[];
    protected $isClients=[];
    protected $isRequisite=[];
    protected $isAddressRequisite=[];
    protected $Formatter;

    /**
     * BaseOdataService constructor.
     * @param FormatterInterface $Formatter
     */
    public function __construct(FormatterInterface $clientsFormatter){
        $this->Formatter=$clientsFormatter;
        $this->odatacfg = new OdataCfg();
        $this->client = new ApiErpOdataClient();
        $this->bitrixclient = new ApiBitirxClient();
    }


}