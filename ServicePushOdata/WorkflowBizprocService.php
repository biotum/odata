<?php

use Bitrix\Main\EventManager;
//use Bitrix\Rest\Api\Events;
use Bitrix\Rest\EventTable;

if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}

if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushDealRequest.php');
}

if (!class_exists('PushFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushFormatterInterface.php');
}
if (!class_exists('PushDealFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushDealFormatter.php');
}
if (!class_exists('PushCompanyFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushCompanyFormatter.php');
}

if (!class_exists('Events')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Events/Events.php');
}
if (!class_exists('Deal')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Deal.php');
}
if (!class_exists('EntityInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/EntityInterface.php');
}


require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');

class WorkflowBizprocService
{

    protected $client;
    private $bitrixclient;
    private $templateID=54;

    /**
     * WorkflowBizprocService constructor.
     * @throws Exception
     *
     */
    public function __construct()
    {
        $this->bitrixclient = new \ApiBitirxClient();

    }

    public function is_Status($event){
        $deal=(new Deal())->getDeal($event['ENTITY_ID']);
        if( $this->instance($event)==0 and $deal['STAGE_ID']=='EXECUTING'){
            $this->start($event);
        }

    }

    public function instance($event)
    {

      $result= json_decode($this->bitrixclient->ApiClient([
            'select'=>['ID', 'MODIFIED', 'OWNED_UNTIL', 'MODULE_ID', 'ENTITY', 'DOCUMENT_ID', 'STARTED', 'STARTED_BY', 'TEMPLATE_ID'],
            'filter'=>['TEMPLATE_ID'=>$this->templateID,'ENTITY'=>'CCrmDocumentDeal','DOCUMENT_ID'=>'DEAL_'.$event['ENTITY_ID']]
        ],'bizproc.workflow.instances'),true);

        $this->bitrixclient->ApiClient(['POST_TITLE'=>'workflow.instances','POST_MESSAGE'=>$result['total'],'DEST'=>['U92']], 'log.blogpost.add');

        return $result['total'];
    }

    public function start($event)
    {
        $this->bitrixclient->ApiClient([ 'TEMPLATE_ID'=>$this->templateID,
            'DOCUMENT_ID'=>['crm', 'CCrmDocumentDeal',$event['ENTITY_ID']],
            'PARAMETERS'=> null],'bizproc.workflow.start');

    }


}