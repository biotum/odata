
<?php

if (!class_exists('PushFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushFormatterInterface.php');
}
if (!class_exists('PushDealFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushDealFormatter.php');
}
if (!class_exists('PushPartnersFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushPartnersFormatter.php');
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quick start. Local server-side application with UI</title>
</head>
<body>
<div id="auth-data">OAuth 2.0 data from REQUEST:
    <PRE>
    <?
    print_r($_REQUEST);
    ?>
    </PRE>
</div>
<div id="name">

    <?

//    (new PushService(new PushPartnersFormatter(), new Partners()))->event('PARTNERS');
//    (new PushService(new PushCompanyFormatter(), new Company()))->event('COMPANY');
//    (new PushService(new PushDealFormatter(), new Deal()))->event('DEAL');
//    (new PushService(new PushBanksRequisiteFormatter(), new BanksRequisite()))->event('BANKDETAIL');
    //print_r((new Events())->pushEvents());
     //BANK_DETAILS[n0][RQ_BIK]
    //(new PushService(new PushPartnersFormatter(),new Partners()))->test();
   (new PushService(new PushBanksRequisiteFormatter(),new BanksRequisite()))->test();
    ?>
</div>
</body>
</html>
