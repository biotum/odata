<?php

if (!class_exists('ClientsFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
}

class PushRequest
{
    private $Client = [];
    private $ClientBank=[];
    private $data;
    private $ID;


    /*
 * "Ref_Key": "6293ac72-83ec-11e7-0696-005056891ac8",
"DataVersion": "AAAAAAASTLE=",
"DeletionMark": false,
"Description": "ЕТС-М ООО",
"ИНН": "7731178271",
"ОбособленноеПодразделение": false,
"ЮридическоеФизическоеЛицо": "ЮридическоеЛицо",
"ГоловнойКонтрагент_Key": "6293ac72-83ec-11e7-0696-005056891ac8",
"КодПоОКПО": "",
"КПП": "773101001",
"НаименованиеПолное": "ООО \"ЕТС-М\"",
"ДополнительнаяИнформация": "",
"Партнер_Key": "df0db559-0e2c-11e8-897d-704d7b7030ea",
"ЮрФизЛицо": "ЮрЛицо",
"НДСПоСтавкам4и2": false,
"СтранаРегистрации_Key": "00000000-0000-0000-0000-000000000000",
"РегистрационныйНомер": "1037700250928",
"НалоговыйНомер": "",
"НаименованиеМеждународное": "",
"КонтактнаяИнформация": [
{
"Ref_Key": "6293ac72-83ec-11e7-0696-005056891ac8",
"LineNumber": "1",
"Тип": "Адрес",
"Вид_Key": "f57765cc-0ccb-11e8-897d-704d7b7030ea",
"Представление": "121351, Москва г, Ивана Франко ул, дом № 48, строение 1",
"ЗначенияПолей": "<КонтактнаяИнформация xmlns=\"http://www.v8.1c.ru/ssl/contactinfo\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" Представление=\"121351, Москва г, Ивана Франко ул, дом № 48, строение 1\"><Комментарий/><Состав xsi:type=\"Адрес\" Страна=\"РОССИЯ\"><Состав xsi:type=\"АдресРФ\"><СубъектРФ>Москва г</СубъектРФ><Улица>Ивана Франко ул</Улица><ДопАдрЭл ТипАдрЭл=\"10100000\" Значение=\"121351\"/><ДопАдрЭл><Номер Тип=\"1010\" Значение=\"48\"/></ДопАдрЭл><ДопАдрЭл><Номер Тип=\"1060\" Значение=\"1\"/></ДопАдрЭл></Состав></Состав></КонтактнаяИнформация>",
"Страна": "РОССИЯ",
"Регион": "Москва г",
"Город": "",
"АдресЭП": "",
"ДоменноеИмяСервера": "",
"НомерТелефона": "",
"НомерТелефонаБезКодов": "",
"ВидДляСписка_Key": "f57765cc-0ccb-11e8-897d-704d7b7030ea",
"ДействуетС": "0001-01-01T00:00:00",
"Значение": ""

/**
 * @param array $data
 */
    public function setClients(array $data): void
    {
        foreach ($data as $value) {

                $this->Client[] = [
                    'Ref_Key' => $value['Ref_Key'],
                    'Description' =>$value['Description'],
                    'НаименованиеПолное' =>$value['НаименованиеПолное'],
                    'ИНН' => $value['ИНН'],
                    'КПП' =>$value['КПП'],
                    'ЮридическоеФизическоеЛицо' =>$value['ЮридическоеФизическоеЛицо'],
                    'КодПоОКПО' =>$value['КодПоОКПО'],
                    'РегистрационныйНомер' =>$value['РегистрационныйНомер'],
                    'Тип' =>$value['Тип'],
                    'Вид_Key' =>$value['Вид_Key'],
                    'Представление' =>$value['Представление'],
                    'ЗначенияПолей' =>$value['ЗначенияПолей'],
                    'Страна' =>$value['Страна'],
                    'Регион' =>$value['Регион'],
                    'Город' =>$value['Город'],
                    'АдресЭП' =>$value['АдресЭП'],
                    'КонтактнаяИнформация'=>$value['КонтактнаяИнформация'],
                    'ВидДляСписка_Key' =>$value['ВидДляСписка_Key'],
                    'ДействуетС' =>$value['ДействуетС'],


                ];




        }

    }

    public function setClientsBanks(array $data): void
    {
        foreach ($data as $value) {

            $this->ClientBank[] =$data;

//                [
//                'Ref_Key' => $value['Owner'],
//                'Description' =>$value['Description'],
//               'КоррСчетБанка' =>$value['КоррСчетБанка'],
//               'Code' => $value['Code'],
//                'КПП' =>$value['КПП'],
//                'ЮридическоеФизическоеЛицо' =>$value['ЮридическоеФизическоеЛицо'],
//                'КодПоОКПО' =>$value['КодПоОКПО'],
//                'РегистрационныйНомер' =>$value['РегистрационныйНомер'],
//                'Тип' =>$value['Тип'],
//                'Вид_Key' =>$value['Вид_Key'],
//                'Представление' =>$value['Представление'],
//                'ЗначенияПолей' =>$value['ЗначенияПолей'],
//                'Страна' =>$value['Страна'],
//                'Регион' =>$value['Регион'],
//                'Город' =>$value['Город'],
//                'АдресЭП' =>$value['АдресЭП'],
//                'КонтактнаяИнформация'=>$value['КонтактнаяИнформация'],
//                'ВидДляСписка_Key' =>$value['ВидДляСписка_Key'],
//                'ДействуетС' =>$value['ДействуетС'],


   //         ];

//http://10.97.21.10/RestApi/odata/standard.odata/Catalog_БанковскиеСчетаКонтрагентов?$select=Ref_Key,Description,Owner,НомерСчета,Банк_Key&$format=json


        }

    }
    /*
     * /
     */

    /**
     * @return array
     */
    public function getClients():array
    {
        return $this->Client;
    }
    /**
     * @return array
     */
    public function getClientsBank():array
    {
        return $this->ClientBank;
    }

}