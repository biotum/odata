<?php

if (!class_exists('ClientsFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
}

class PushDealRequest
{

    private $push= array();
    private $deal=[];
    private $product=[];
    private $deliveryproduct=[];
    private $dealproduct=[];
    private $client=[];
    private $consignee=[];
    private $store=[];
    private $addressdelivery;
    private $ORIGINID;
    private $DEAL_ID;
    private$TITLE;
    private$OPPORTUNITY;
    private$ASSIGNED_BY_NAME;
    private$DATA_CREATE;
    private$TYPE;
    private$PAYER;
    private$CONSIGNEEID;
    private$COMMENTS;
    private$PRODUCTID;
    private$PRICE;
    private$QUANTITY;
    private$NDSSUMM;
    private$UPLOADINGPOINTS=[];
    private $ADDRESSSTORE;
    private $ONWERKEY;
    private $guid;
    private $requisite;
    private $addressrequisite=[];
    private $banksrequisite=[];

    /**
     * @return array
     */
    public function getBanksrequisite(): array
    {
        return $this->banksrequisite;
    }

    /**
     * @param array $banksrequisite
     */
    public function setBanksrequisite(array $banksrequisite): void
    {
        $this->banksrequisite = $banksrequisite;
    }
    /**
     * @return mixed
     */
    public function getAddressrequisite()
    {
        return $this->addressrequisite;
    }

    /**
     * @param mixed $addressrequisite
     */
    public function setAddressrequisite(array $addressrequisite): void
    {
        $this->addressrequisite = $addressrequisite;
    }

    /**
     * @return mixed
     */
    public function getRequisite()
    {
        return $this->requisite;
    }

    /**
     * @param mixed $requisite
     */
    public function setRequisite($requisite): void
    {
        $this->requisite = $requisite;
    }

    /**
     * @return mixed
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param mixed $guid
     */
    public function setGuid($guid): void
    {
        $this->guid = $guid;
    }

    /**
     * @return mixed
     */
    public function getONWERKEY()
    {
        return $this->ONWERKEY;
    }

    /**
     * @param mixed $ONWERKEY
     */
    public function setONWERKEY(string $ONWERKEY): void
    {
        $this->ONWERKEY = $ONWERKEY;
    }

    /**
     * @return mixed
     */
    public function getADDRESSSTORE()
    {
        return $this->ADDRESSSTORE;
    }

    /**
     * @param mixed $ADDRESSSTORE
     */
    public function setADDRESSSTORE($ADDRESSSTORE): void
    {
        $this->ADDRESSSTORE = $ADDRESSSTORE;
    }


    /**
     * @return array
     */
    public function getDeal(): array
    {
        return $this->deal;
    }

    /**
     * @param array $deal
     */
    public function setDeal(array $deal): void
    {
        $this->deal = $deal;
    }

    /**
     * @return array
     */
    public function getProduct(): array
    {
        return $this->product;
    }

    /**
     * @param array $product
     */
    public function setProduct(array $product): void
    {
        $this->product = $product;
    }

    /**
     * @return array
     */
    public function getDeliveryproduct(): array
    {
        return $this->deliveryproduct;
    }

    /**
     * @param array $deliveryproduct
     */
    public function setDeliveryproduct(array $deliveryproduct): void
    {
        $this->deliveryproduct = $deliveryproduct;
    }

    /**
     * @return array
     */
    public function getDealproduct(): array
    {
        return $this->dealproduct;
    }

    /**
     * @param array $dealproduct
     */
    public function setDealproduct(array $dealproduct): void
    {
        $this->dealproduct = $dealproduct;
    }

    /**
     * @return array
     */
    public function getClient(): array
    {
        return $this->client;
    }

    /**
     * @param array $client
     */
    public function setClient(array $client): void
    {
        $this->client = $client;
    }

    /**
     * @return array
     */
    public function getConsignee(): array
    {
        return $this->consignee;
    }

    /**
     * @param array $consignee
     */
    public function setConsignee(array $consignee): void
    {
        $this->consignee = $consignee;
    }

    /**
     * @return mixed
     */
    public function getCONSIGNEEID()
    {
        return $this->CONSIGNEEID;
    }

    /**
     * @param mixed $CONSIGNEEID
     */
    public function setCONSIGNEEID($CONSIGNEEID): void
    {
        $this->CONSIGNEEID = $CONSIGNEEID;
    }

    /**
     * @return mixed
     */
    public function getPRODUCTID()
    {
        return $this->PRODUCTID;
    }

    /**
     * @param mixed $PRODUCTID
     */
    public function setPRODUCTID($PRODUCTID): void
    {
        $this->PRODUCTID = $PRODUCTID;
    }



    /**
     * @return array
     */
    public function getStore(): array
    {
        return $this->store;
    }

    /**
     * @param array $store
     */
    public function setStore(array $store): void
    {
        $this->store = $store;
    }

    /**
     * @return mixed
     */
    public function getAddressdelivery()
    {
        return $this->addressdelivery;
    }

    /**
     * @param mixed $addressdelivery
     */
    public function setAddressdelivery($addressdelivery): void
    {
        $this->addressdelivery = $addressdelivery;
    }

    /**
     * @return mixed
     */
    public function getORIGINID()
    {
        return $this->ORIGINID;
    }

    /**
     * @param mixed $ORIGINID
     */
    public function setORIGINID($ORIGINID)
    {
        $this->ORIGINID = $ORIGINID;
    }



    public function getDelivery(): array
    {
        $delivery=[];

        foreach ($this->deliveryproduct as $item=>$value){

            $delivery[]=[
             //   'Ref_Key'=>'a50e52c6-e9ba-11e9-9056-00155d01050d', //$this->deliveryproduct[$item]['ID'],
                'LineNumber'=>$item+1,
                'АдресДоставки_Key'=>$this->store[$item][0][0]['XML_ID'],
                'Вес'=>$this->deliveryproduct[$item]['WEIGHT'],
                'ДатаДоставки'=>$this->deliveryproduct[$item]['DATE_DELIVERY']?$this->deliveryproduct[$item]['DATE_DELIVERY']:'0001-01-01T00:00:00',
                'ВремяДоставки'=>$this->deliveryproduct[$item]['TIME_DELIVERY']?'0001-01-01T'.$this->deliveryproduct[$item]['TIME_DELIVERY'].':00':'0001-01-01T:00',
            ];
        }
        return $delivery;
    }

    /**
     * @param array $push
     */
    public function getPush(): void
    {
            //$this->setORIGINID($this->deal['ORIGIN_ID']);
            $this->setDEALID($this->deal['ID']);
            $this->setTITLE($this->deal['TITLE']);
            $this->setOPPORTUNITY($this->deal['OPPORTUNITY']);
            $this->setASSIGNEDBYNAME($this->getUserFullName($this->deal['ASSIGNED_BY_ID']));
            $this->setDATACREATE($this->deal['DATE_CREATE']);
            $this->setTYPE($this->deal['TYPE_ID']);
            $this->setPAYER($this->client['ORIGIN_ID']);
            $this->setCONSIGNEEID($this->consignee['ORIGIN_ID']);
            $this->setCOMMENTS($this->deal['COMMENTS']);
            $this->setPRODUCTID($this->product['XML_ID']);
            $this->setPRICE($this->dealproduct['PRICE']);
            $this->setQUANTITY($this->dealproduct['QUANTITY']);
            $this->setNDSSUMM($this->deal['OPPORTUNITY']);
            $this->setUPLOADINGPOINTS($this->getDelivery());

    }

    /**
     * @param array $push
     */
    public function getPushStore(): void
    {

    }

    private function getUserFullName($user_id){
        $rs_user = CUser::GetByID($user_id);
        $user = $rs_user->Fetch();

        return $user["NAME"].' '. $user["LAST_NAME"];
    }


    /**
     * @return mixed
     */
    public function getDEALID()
    {
        return $this->DEAL_ID;
    }

    /**
     * @param mixed $DEAL_ID
     */
    public function setDEALID($DEAL_ID): void
    {
        $this->DEAL_ID = $DEAL_ID;
    }

    /**
     * @return mixed
     */
    public function getTITLE()
    {
        return $this->TITLE;
    }

    /**
     * @param mixed $TITLE
     */
    public function setTITLE($TITLE): void
    {
        $this->TITLE = $TITLE;
    }

    /**
     * @return mixed
     */
    public function getOPPORTUNITY()
    {
        return $this->OPPORTUNITY;
    }

    /**
     * @param mixed $OPPORTUNITY
     */
    public function setOPPORTUNITY($OPPORTUNITY): void
    {
        $this->OPPORTUNITY = $OPPORTUNITY;
    }

    /**
     * @return mixed
     */
    public function getASSIGNEDBYNAME()
    {
        return $this->ASSIGNED_BY_NAME;
    }

    /**
     * @param mixed $ASSIGNED_BY_NAME
     */
    public function setASSIGNEDBYNAME($ASSIGNED_BY_NAME): void
    {
        $this->ASSIGNED_BY_NAME = $ASSIGNED_BY_NAME;
    }

    /**
     * @return mixed
     */
    public function getDATACREATE()
    {
        return $this->DATA_CREATE;
    }

    /**
     * @param mixed $DATA_CREATE
     */
    public function setDATACREATE($DATA_CREATE): void
    {
        $this->DATA_CREATE = $DATA_CREATE;
    }

    /**
     * @return mixed
     */
    public function getTYPE()
    {
        return $this->TYPE;
    }

    /**
     * @param mixed $TYPE
     */
    public function setTYPE($TYPE): void
    {
        $this->TYPE = $TYPE;
    }

    /**
     * @return mixed
     */
    public function getPAYER()
    {
        return $this->PAYER;
    }

    /**
     * @param mixed $PAYER
     */
    public function setPAYER($PAYER): void
    {
        $this->PAYER = $PAYER;
    }

    /**
     * @return mixed
     */
    public function getCOMMENTS()
    {
        return $this->COMMENTS;
    }

    /**
     * @param mixed $COMMENTS
     */
    public function setCOMMENTS($COMMENTS): void
    {
        $this->COMMENTS = $COMMENTS;
    }

    /**
     * @return mixed
     */
    public function getPRICE()
    {
        return $this->PRICE;
    }

    /**
     * @param mixed $PRICE
     */
    public function setPRICE($PRICE): void
    {
        $this->PRICE = $PRICE;
    }

    /**
     * @return mixed
     */
    public function getQUANTITY()
    {
        return $this->QUANTITY;
    }

    /**
     * @param mixed $QUANTITY
     */
    public function setQUANTITY($QUANTITY): void
    {
        $this->QUANTITY = $QUANTITY;
    }

    /**
     * @return mixed
     */
    public function getNDSSUMM()
    {
        return $this->NDSSUMM;
    }

    /**
     * @param mixed $NDSSUMM
     */
    public function setNDSSUMM($NDSSUMM): void
    {
        $this->NDSSUMM = $NDSSUMM;
    }

    /**
     * @return mixed
     */
    public function getUPLOADINGPOINTS()
    {
        return $this->UPLOADINGPOINTS;
    }

    /**
     * @param mixed $UPLOADINGPOINTS
     */
    public function setUPLOADINGPOINTS(array $UPLOADINGPOINTS): void
    {
        $this->UPLOADINGPOINTS = $UPLOADINGPOINTS;
    }




}