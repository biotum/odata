<?php

use Bitrix\Main\ArgumentException;
use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\Type\DateTime;
use Bitrix\Rest\AccessException;
use Bitrix\Rest\EventOfflineTable;
use Bitrix\Rest\EventTable;
use Bitrix\Rest\OAuth\Auth;
use Bitrix\Main;

if (!class_exists('eventoffline')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/rest/lib/eventoffline.php');
}
if (!class_exists('rest_util')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/rest/classes/general/rest_util.php');
}
if (!class_exists('auth')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/rest/lib/oauth/auth.php');
}


class  Events
{
    protected $bitrixclient;

    public function __construct()
    {
        $this->cfg= new Odatacfg();
    }

    public function Create(){

    }

    public function Delete(){


    }

    /**
     * @return array
     */
    public function pushEvents(){
        $list=[];
        foreach($this->EventsLists() as $eventsList){

            if($this->cfg::EVENTS[$eventsList['EVENT_NAME']]) {
                $list[] = [
                    'MESSAGE_ID' => $eventsList['MESSAGE_ID'],
                    'EVENT_NAME' => $eventsList['EVENT_NAME'],
                    'EVENT_ID' => $eventsList['EVENT_ID'],
                    'URL' => $this->cfg::EVENTS[$eventsList['EVENT_NAME']]['url'],
                //    'URL2' => $this->cfg::EVENTS[$eventsList['EVENT_NAME']]['url2'],
                    'METOD' => $this->cfg::EVENTS[$eventsList['EVENT_NAME']]['metod'],
                    'ENTITY_ID' => $eventsList['ID']['ID'],
                    'ENTITY_USER' => $eventsList['USER_ID']];
            }
            if($eventsList['EVENT_NAME']==='ONCRMCOMPANYUPDATE' or $eventsList['EVENT_NAME']==='ONCRMCOMPANYADD' ){
                $list[] = [
                    'MESSAGE_ID' =>null,
                    'EVENT_NAME' =>'PARTNERS',
                    'EVENT_ID' => $eventsList['EVENT_ID'],
                    'URL' => $this->cfg::PARTNERS,
                 //   'URL2' => $this->cfg::EVENTS[$eventsList['EVENT_NAME']]['url2'],
                    'METOD' =>$this->cfg::EVENTS[$eventsList['EVENT_NAME']]['metod'],
                    'ENTITY_ID' => $eventsList['ID']['ID'],
                    'ENTITY_USER' => $eventsList['USER_ID']];
            }
        }

        return  $list;
    }

    public function entityEvent($eventname){
        $eventname=str_replace('ON','',$eventname);
        $eventname=str_replace('ADD','',$eventname);
        $eventname=str_replace('UPDATE','',$eventname);
        $eventname=str_replace('CRM','',$eventname);

     return $eventname;
    }


    public function pushEntityEvents(){

    }

    /**
     * @param $query
     * @return bool
     * @throws AccessException
     * @throws ArgumentException
     * @throws ArgumentNullException
     */
    public function EventsListsClear($message_id)
    {



        $connection = Main\Application::getConnection();
        $helper = $connection->getSqlHelper();

        $tableName = 'b_rest_event_offline';

        $connectorId = $connection->getSqlHelper()->forSql($message_id);

        $sql = "DELETE FROM {$tableName} WHERE MESSAGE_ID='{$message_id}'";


        $connection->query($sql);


        return true;
    }


    /**
     * @return array
     * @throws ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function EventsLists(){

        $dbRes = EventOfflineTable::getList(array(
            'select' => array(
                'ID', 'TIMESTAMP_X', 'EVENT_NAME', 'EVENT_DATA', 'EVENT_ADDITIONAL', 'MESSAGE_ID'
            ),

        ));

        $result = array();

        while($event = $dbRes->fetch())
        {
            /** @var DateTime $ts */
            $ts = $event['TIMESTAMP_X'];

            $event['TIMESTAMP_X'] = \CRestUtil::convertDateTime($ts->toString());
            $event['EVENT_ADDITIONAL'] = array(
                'user_id' => $event['EVENT_ADDITIONAL'][Auth::PARAM_LOCAL_USER],
            );

            $result[] =['ID'=>$event['EVENT_DATA']['FIELDS'],
                'MESSAGE_ID'=>$event['MESSAGE_ID'],
                'EVENT_NAME'=>$event['EVENT_NAME'],
                'USER_ID'=>$event['EVENT_ADDITIONAL']['user_id'],
                'EVENT_ID'=>$event['ID']
            ];

        }


        return $result;
    }

    /**
     * @return array
     * @throws ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getEvents(){

        global $USER;


        $result = array();

        $dbRes =EventTable::getList(array(
            'order' => array(
                "ID" => "ASC",
            ),
        ));
        while($eventHandler = $dbRes->fetch())
        {
            if(strlen($eventHandler['EVENT_HANDLER']) > 0)
            {
                $result[] = array(
                    "event" => $eventHandler['EVENT_NAME'],
                    "handler" => $eventHandler['EVENT_HANDLER'],
                    "auth_type" => $eventHandler['USER_ID'],
                    "offline" => 0
                );
            }
            else
            {
                $result[] = array(
                    "event" => $eventHandler['EVENT_NAME'],
                    "connector_id" => $eventHandler['CONNECTOR_ID'] === null ? '' : $eventHandler['CONNECTOR_ID'],
                    "offline" => 1
                );
            }
        }

        return $result;

    }

}