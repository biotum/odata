<?php

use Bitrix\Main\EventManager;
//use Bitrix\Rest\Api\Events;
use Bitrix\Rest\EventTable;

if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}

if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushDealRequest.php');
}

if (!class_exists('PushFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushFormatterInterface.php');
}
if (!class_exists('PushDealFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushDealFormatter.php');
}
if (!class_exists('PushCompanyFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushCompanyFormatter.php');
}

if (!class_exists('Events')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Events/Events.php');
}
if (!class_exists('Deal')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Deal.php');
}
if (!class_exists('EntityInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/EntityInterface.php');
}
if (!class_exists('PushDealFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushDealFormatter.php');
}
if (!class_exists('PushStoreFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushStoreFormatter.php');
}
if (!class_exists('PushService')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushService.php');
}
if (!class_exists('PushEvents')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Events/PushEvents.php');
}
if (!class_exists('Deal')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Deal.php');
}
if (!class_exists('Company')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Company.php');
}
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');
class PushEvents
{



    public static function onBeforeStoreUpdate(Bitrix\Main\Event $event)
    {
        $param=$event->getParameters();

        $list=[
            'MESSAGE_ID'=>'',
            'EVENT_NAME'=>'',
            'EVENT_ID'=>'',
            'URL'=>(new Odatacfg())::ADDRESSSTORE,
            'METOD'=>'POST',
            'ENTITY_ID'=>$param['id'],
            'ENTITY_USER'=>'',
            'CONSEGNEE'=>null
        ];

       (new PushService(new PushStoreFormatter(),new Store()))->pushOdata($list);

     //   $bitrixclient = new ApiBitirxClient();
        //echo 'Параметры, переданные в обработчик';

     // print_r(json_decode($bitrixclient->ApiClient(['POST_TITLE'=>'onBeforeStoreUpdate','POST_MESSAGE'=>json_encode(),'DEST'=>['U92']], 'log.blogpost.add'),true));

        return false;

    }
    public static function onBeforeStoreAdd(Bitrix\Main\Event $event)
    {

        return false;

    }

    public static function onBeforeClientsStoreAdd(Bitrix\Main\Event $event)
    {
        $param=$event->getParameters();
        $company=new Company();
        $guid=$company->getCompany($param['company']);

        $list=[
            'MESSAGE_ID'=>'',
            'EVENT_NAME'=>'',
            'EVENT_ID'=>'',
            'URL'=>(new Odatacfg())::ADDRESSSTORE,
            'METOD'=>'POST',
            'ENTITY_ID'=>$param['id'],
            'ENTITY_USER'=>'',
            'CONSEGNEE'=>$guid['ORIGIN_ID']
        ];

   //     $response = (new PushService(new PushStoreFormatter(),new Store()))->pushOdata($list);

        $bitrixclient = new ApiBitirxClient();
        //echo 'Параметры, переданные в обработчик';

      $bitrixclient->ApiClient(['POST_TITLE'=>'onBeforeClientsStoreAdd','POST_MESSAGE'=>'fsdfd','DEST'=>['U92']], 'log.blogpost.add');

        return false;

    }

    public static function onBeforeClientsStoreUpdate(Bitrix\Main\Event $event)
    {

        $param=$event->getParameters();
        $company=new Company();
        $guid=$company->getCompany($param['company']);

        $list=[
            'MESSAGE_ID'=>'',
            'EVENT_NAME'=>'',
            'EVENT_ID'=>'',
            'URL'=>(new Odatacfg())::ADDRESSSTORE,
            'METOD'=>'POST',
            'ENTITY_ID'=>$param['id'],
            'ENTITY_USER'=>'',
            'CONSEGNEE'=>$guid['ORIGIN_ID']
        ];

        $response = (new PushService(new PushStoreFormatter(),new Store()))->pushOdata($list);

        $bitrixclient = new ApiBitirxClient();

        print_r(json_decode($bitrixclient->ApiClient(['POST_TITLE'=>'onBeforeClientsStoreUpdate','POST_MESSAGE'=>json_encode($response),'DEST'=>['U92']], 'log.blogpost.add'),true));

        return false;

    }

}