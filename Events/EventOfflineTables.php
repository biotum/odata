<?php
namespace Bitrix\Main;


use Bitrix\Main\Entity;

/**
 * Class EventTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> APP_ID int mandatory
 * <li> EVENT_NAME string(255) mandatory
 * <li> EVENT_HANDLER string(255) mandatory
 * <li> USER_ID int optional
 * </ul>
 *
 * @package
 **/
class EventOfflineTable  extends Entity\DataManager
{
    const PROCESS_ID_LIFETIME = 2952000; // 30 days

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_rest_event_offline';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'TIMESTAMP_X' => array(
                'data_type' => 'datetime',
            ),
            'MESSAGE_ID' => array(
                'data_type' => 'string',
                'required' => true,
            ),
            'APP_ID' => array(
                'data_type' => 'integer',
                'required' => true,
            ),
            'EVENT_NAME' => array(
                'data_type' => 'string',
                'required' => true,
            ),
            'EVENT_DATA' => array(
                'data_type' => 'text',
                'serialized' => true,
            ),
            'EVENT_ADDITIONAL' => array(
                'data_type' => 'text',
                'serialized' => true,
            ),
            'PROCESS_ID' => array(
                'data_type' => 'string',
            ),
            'CONNECTOR_ID' => array(
                'data_type' => 'string',
            ),
            'ERROR' => array(
                'data_type' => 'integer',
            ),
        );
    }
}