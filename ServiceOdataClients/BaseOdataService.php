<?php


if (!class_exists('OdataCfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/Odatacfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ApiErp/ApiErpOdataClient.php');
}

if (!class_exists('ClientsRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}
if (!class_exists('FormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
}

class BaseOdataService
{
    protected $odatacfg;
    protected $client;
    protected $bitrixclient;
    protected $featchrequest;
    protected $valuesearch;
    protected $featchClientsrequest;
    protected $listUpdate = [];
    protected $listClientsUpdate = [];
    protected $listRequisiteUpdate = [];
    protected $listClients = [];
    protected $listRequisite = [];
    protected $listAddressRequisite = [];
    protected $listBanksRequisite =[];
    protected $Clientslist=[];
    protected $isClients=[];
    protected $isRequisite=[];
    protected $isAddressRequisite=[];
    protected $isBanksRequisite=[];
    protected $Formatter;

    /**
     * BaseOdataService constructor.
     * @param FormatterInterface $Formatter
     */
    public function __construct(FormatterInterface $clientsFormatter){
        $this->Formatter=$clientsFormatter;
        $this->odatacfg = new OdataCfg();
        $this->client = new ApiErpOdataClient();
        $this->bitrixclient = new ApiBitirxClient();
        $this->featchClientrequest = new ClientsRequest();

    }

    /**
     * @param array $field
     * @return array|mixed
     */
    protected function getClientslist($field=[])
    {

        if (empty($this->isClients)) {
            if ($field) {
                $data = ["auth" => $_REQUEST['AUTH_ID'], 'select' => $field];

            } else {
                $data = $this->Formatter->Formatlist();

            }

            foreach ($this->bitrixclient->getlistApiClient($data, 'crm.company.list') as $res) {
                $this->isClients[] = $res['result'];
            }
            $this->isClients = call_user_func_array('array_merge', $this->isClients);
        }
     return  $this->isClients;
    }

    /**
     * @param array $field
     * @return array|mixed
     */
    protected function getRequisitelist($field=[])
    {

            if ($field) {
                $data = ["auth" => $_REQUEST['AUTH_ID'], 'select' => $field,'filter'=>['!XML_ID'=>false]];

            } else {
                $data = $this->Formatter->Formatlist();

            }

            foreach ($this->bitrixclient->getlistApiClient($data, 'crm.requisite.list') as $res) {
                $this->isRequisite[] = $res['result'];
            }
            $this->isRequisite = call_user_func_array('array_merge', $this->isRequisite);

        return    $this->isRequisite ;
    }

    /**
     * @param array $field
     * @return array|mixed
     */
    protected function getAddressRequisitelist($field=[])
    {

        if ($field) {
            $data = ["auth" => $_REQUEST['AUTH_ID'], 'select' => $field];

        } else {
            $data = $this->Formatter->Formatlist();

        }

        foreach ($this->bitrixclient->getlistApiClient($data, 'crm.address.list') as $res) {


            $this->isAddressRequisite[] = $res['result'];
        }
        $this->isAddressRequisite = call_user_func_array('array_merge', $this->isAddressRequisite);

        return   call_user_func_array('array_merge', $this->isAddressRequisite);
    }

    /**
     * @param array $field
     * @return array|mixed
     */
    protected function getBanksRequisitelist($field=[])
    {

        if ($field) {
            $data = ["auth" => $_REQUEST['AUTH_ID'], 'select' => $field];

        } else {
            $data = $this->Formatter->Formatlist();

        }

        foreach ($this->bitrixclient->getlistApiClient($data, 'crm.requisite.bankdetail.list') as $res) {


            $this->isBanksRequisite[] = $res['result'];
        }

        $this->isBanksRequisite = call_user_func_array('array_merge', $this->isBanksRequisite);

        return    $this->isBanksRequisite;
    }
    /**
     * @param $products
     * @param $field
     * @param $value
     * @return bool
     */
    protected function inFind($array,$value)
    {
        $this->valuesearch=$value;
        $result=array_filter($array,function ($x){
            return $x['ORIGIN_ID']===$this->valuesearch;
        });
        if(!empty($result))
            return true;

    }
}