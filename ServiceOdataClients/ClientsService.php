<?php


use Bitrix\Main\EventManager;
use Bitrix\Rest\Api\Events;
use Bitrix\Rest\EventTable;

if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}

if (!class_exists('ClientsRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

if (!class_exists('FormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
}

if (!class_exists('RequisiteFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/RequisiteFormatter.php');
}
if (!class_exists('AddressRequisiteFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/AddressRequisiteFormatter.php');
}
if (!class_exists('ClientsBankAccountFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsBankAccountFormatter.php');
}
if (!class_exists('BaseOdataService')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/BaseOdataService.php');
}
//if (!class_exists('Events')) {
//    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Events/Events.php');
//}
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');


class ClientsService extends BaseOdataService
{


    private $requisite = [];

    /**
     *
     */
    public function FetchClients()
    {
        $array = [];

        try {

            $response = $this->client->RestApiClient($this->odatacfg::CLIENTS, ['$format' => 'json']);
            $this->featchClientrequest->setClients($response['value']);
            $this->client->close();

        } catch (Exception $exception) {

            var_dump($exception);

        }

    }

    public function FetchBankAccount()
    {
        try {
            $response = $this->client->RestApiClient($this->odatacfg::BANKSACCOUNT, [
                '$select' => 'Ref_Key,Owner,Description,НомерСчета,Банк',
                '$expand' => '*',
                '$format' => 'json']);

            $this->featchClientrequest->setClientsBanks($response['value']);

            $this->client->close();

        } catch (Exception $exception) {

            var_dump($exception);

        }

    }

    /**
     *
     */
    public function SaveRequisite()
    {
        $array = [];

        try {

            $this->FetchClients();
            foreach ($this->Formatter->Format($this->featchClientrequest) as $value) {
                $this->listRequisite[] = $value['fields'];
            }
            $this->getRequisitelist();
            $searchid = null;
            foreach ($this->getClientslist() as $item => $company) {

                $id = array_search($company['ORIGIN_ID'], array_column($this->isRequisite, 'XML_ID'));
                $this->listRequisite[$id]['ENTITY_ID'] = $company['ID'];
                if ($id !== false and $this->listRequisite[$id]['ENTITY_ID'] == $company['ID']) {
                    // if (!(boolean)$id) {
                    $searchid = array_search($company['ORIGIN_ID'], array_column($this->listRequisite, 'XML_ID'));
                    $this->listRequisite[$searchid]['ENTITY_ID'] = $company['ID'];
                    $this->bitrixclient->ApiClient($this->Formatter->FormatUpdate($this->isRequisite[$id]['ID'], $this->listRequisite[$searchid]), 'crm.requisite.update');
                } else {
                    $searchid = array_search($company['ORIGIN_ID'], array_column($this->listRequisite, 'XML_ID'));
                    $this->listRequisite[$searchid]['ENTITY_ID'] = $company['ID'];
                    $this->bitrixclient->ApiClient($this->Formatter->FormaInsert($this->listRequisite[$searchid]), 'crm.requisite.add');


                }
            }

            $this->client->close();

        } catch (Exception $exception) {

            var_dump($exception);

        }

    }

    /**
     *
     */
    public function SaveAddressRequisite()
    {
        $array = [];

        try {

            $this->FetchClients();
            foreach ($this->Formatter->Format($this->featchClientrequest) as $address) {
                $this->listAddressRequisite[] = $address['fields'];
            }
          //   print_r(json_decode($this->bitrixclient->ApiClient(["auth" => $_REQUEST['AUTH_ID'],'select'=>['*']], 'crm.address.list'),true));
            $this->getAddressRequisitelist();
            $searchid = null;

            foreach ($this->getRequisitelist() as $item => $requisite) {
                $id = array_search($requisite['ID'], array_column($this->isAddressRequisite, 'ENTITY_ID'));
                $this->listAddressRequisite[$id]['ENTITY_ID'] = $requisite['ID'];
                $this->listAddressRequisite[$id]['ANCHOR_ID'] = $requisite['ENTITY_ID'];
                if (!(boolean)$id) {
                    $searchid = array_search($requisite['XML_ID'], array_column($this->listAddressRequisite, 'XML_ID'));
                    $this->listAddressRequisite[$searchid]['ENTITY_ID'] = $requisite['ID'];
                    $this->listAddressRequisite[$searchid]['ANCHOR_ID'] = $requisite['ENTITY_ID'];
                   // print_r ($this->Formatter->FormaInsert($this->listAddressRequisite[$searchid]));
                   $this->bitrixclient->ApiClient($this->Formatter->FormaInsert($this->listAddressRequisite[$searchid]), 'crm.address.add');


                } else {
                    $searchid = array_search($requisite['XML_ID'], array_column($this->listAddressRequisite, 'XML_ID'));
                    $this->listAddressRequisite[$searchid]['ENTITY_ID'] = $requisite['ID'];
                    $this->listAddressRequisite[$searchid]['ANCHOR_ID'] = $requisite['ENTITY_ID'];
                    //   print_r(json_decode($this->bitrixclient->ApiClient(AddressRequisiteFormatter::FormatUpdate('',$this->listAddressRequisite[$searchid]), 'crm.address.delete'),true));
                   $this->bitrixclient->ApiClient($this->Formatter->FormatUpdate('', $this->listAddressRequisite[$searchid]), 'crm.address.update');
               //   print_r ($this->Formatter->FormatUpdate('', $this->listAddressRequisite[$searchid]));


                }
            }
            print_r($this->isAddressRequisite);
            $this->client->close();

        } catch (Exception $exception) {

            var_dump($exception);

        }

    }

    /**
     * @param $response
     */
    public function SaveClients()
    {

        $this->FetchClients();
        foreach ($this->Formatter->Format($this->featchClientrequest) as $value) {
            $this->listClients[] = $value['fields'];
        }
        foreach ($this->Formatter->Format($this->featchClientrequest) as $client) {

            if (!$this->isClientsInsertlist($client['fields']['ORIGIN_ID'])) {

                $this->bitrixclient->ApiClient($client, 'crm.company.add');

            } else {

                $this->listClientsUpdate[] = $this->Formatter->FormatlistUpdate($client);
            }
        }
        if (!empty($this->listClientsUpdate)) {
            foreach ($this->getClientsUpdatelist() as $item => $value) {

                $this->bitrixclient->ApiClient($this->Formatter->FormatUpdate($value['ID'], $value), 'crm.company.update');
            }

        }

        $this->client->close();

    }

    /**
     * @param $response
     */
    public function SaveClientsBankAccount()
    {

        try {

            $this->FetchBankAccount();
            foreach ($this->Formatter->Format($this->featchClientrequest) as $banks) {
                $this->listBanksRequisite[] = $banks['fields'];
            }
            $this->getBanksRequisitelist();
            $searchid = null;

            foreach ($this->getRequisitelist() as $item => $requisite) {

                $searchupdateid = array_keys(array_column($this->listBanksRequisite, 'ORIGINATOR_ID'), $requisite['XML_ID']);
                foreach ($searchupdateid as $value) {
                    $this->listBanksRequisite[$value]['ENTITY_ID'] = $requisite['ID'];

                }
            }

            foreach ($this->listBanksRequisite as $item => $featchreqisite) {
                $ids = array_search($featchreqisite['XML_ID'], array_column($this->isBanksRequisite, 'XML_ID'));
                if ($ids !== false and $this->isBanksRequisite[$ids]['XML_ID'] == $featchreqisite['XML_ID']) {
                    //      print_r($this->Formatter->FormatUpdate($this->isBanksRequisite[$ids]['ID'],$featchreqisite));
                    $this->bitrixclient->ApiClient($this->Formatter->FormatUpdate($this->isBanksRequisite[$ids]['ID'], $featchreqisite), 'crm.requisite.bankdetail.update');

                } else {
                    //          print_r($this->Formatter->FormaInsert($featchreqisite));
                    $this->bitrixclient->ApiClient($this->Formatter->FormaInsert($featchreqisite), 'crm.requisite.bankdetail.add');
                }

            }

            $this->client->close();
        } catch (Exception $exception) {

            var_dump($exception);

        }
    }


    /**
     * @param $isSection
     * @return bool
     */
    private function isClientsInsertlist($value)
    {

        return $this->inFind($this->getClientslist(), $value);
    }

    /**
     * @return array
     */
    private function getClientsUpdatelist()
    {

        foreach ($this->isClients as $elm) {

            foreach ($this->listClientsUpdate as $item => $grp) {

                if ($elm['ORIGIN_ID'] === $grp['ORIGIN_ID']) {


                    $this->listClientsUpdate[$item]['ID'] = $elm['ID'];

                }

            }

        }

        return $this->listClientsUpdate;
    }

    public function test()
    {

        // $this->getAddressRequisitelist();

        // print_r($this->isAddressRequisite);
        // $this->getRequisitelist();
        //  print_r( $this->isRequisite);
        $bitrixclient = new ApiBitirxClient();
        print_r($bitrixclient->getlistApiClient([], 'disk.storage.getlist'));

    }
}