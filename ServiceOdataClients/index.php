
<?php

use Bitrix\Rest\EventTable;

if (!class_exists('ClientsService')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsService.php');
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quick start. Local server-side application with UI</title>
</head>
<body>
<div id="auth-data">OAuth 2.0 data from REQUEST:
    <PRE>
    <?
    print_r($_REQUEST);
    ?>
    </PRE>
</div>
<div id="name">

    <?
  (new ClientsService(new ClientsFormatter()))->SaveClients();
   (new ClientsService(new RequisiteFormatter()))->SaveRequisite();
   (new ClientsService(new AddressRequisiteFormatter()))->SaveAddressRequisite();
  (new ClientsService(new ClientsBankAccountFormatter()))->SaveClientsBankAccount();

 //   (new ClientsService(new AddressRequisiteFormatter()))->test();


    ?>
</div>
</body>
</html>

