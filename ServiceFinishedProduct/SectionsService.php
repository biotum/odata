<?php


if (!class_exists('Cfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('FinishedProductSectionRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/FinishedProductSectionRequest.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/FinishedProductRequest.php');
}
if (!class_exists('FinishedProductFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductSectionFormatterInterface.php');
}
if (!class_exists('FinishedProductFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductSectionFormatter.php');
}
if (!class_exists('Service')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/Service.php');
}
class SectionsService extends Service
{

    /**
     *
     */
    public function SaveSections()
    {

        try {
            $this->SectionUpdate($this->client->RestApiClient($this->cfg::CATALOGPRODUCT,[]));
            $this->client->close();
        }catch (Exception $exception)
        {
            var_dump($exception);
        }
    }

    /**
     * @param $response
     */
    private function SectionUpdate($response)
    {

        $this->featchrequest->setSections($response['value']);

        $this->client->close();

        foreach (FinishedProductSectionFormatter::FormatAdd($this->featchrequest) as $value) {

            if (!$this->isSectionInsertlist($value['fields']['XML_ID'])) {

               $this->bitrixclient->ApiClient($value, 'crm.productsection.add');
            }
            else{
                $this->listUpdate[]=['ID'=>'','XML_ID'=>$value['fields']['XML_ID'],'NAME'=>$value['fields']['NAME']];
            }
      }

        foreach ($this->SectionUpdatelist() as $item=>$value){
            $this->bitrixclient->ApiClient(FinishedProductSectionFormatter::FormatUpdate($value['ID'],$value['NAME']), 'crm.productsection.update');
        }


        $this->client->close();
        unset($this->listUpdate);

    }

    /**
     * @param $isSection
     * @return bool
     */
    private function isSectionInsertlist($value){

        return $this->inFind($this->getSectionlist(),$value);
    }

    /**
     * @return array
     */
    private function SectionUpdatelist(){

        foreach($this->getSectionlist( ["ID","XML_ID"]) as $elm) {

            foreach($this->listUpdate as $item=>$grp) {

                if ($elm['XML_ID'] === $grp['XML_ID']) {

                    $this->listUpdate[$item]['ID']=$elm['ID'];
                }

            }

        }
        return $this->listUpdate;
    }



}