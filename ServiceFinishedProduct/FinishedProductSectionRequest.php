<?php

if (!class_exists('FinishedProductFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductSectionFormatterInterface.php');
}
if (!class_exists('FinishedProductFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductSectionFormatter.php');
}

class FinishedProductSectionRequest
{
    private $Section = [];
    private $GrouppSection = [];
    private $data;
    private $ID;
    private $XML_ID;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID): void
    {
        $this->ID = $ID;
    }

    /**
     * @return array
     */
    public function getSections(): array
    {
        return $this->Section;
        //array_map("unserialize", array_unique(array_map("serialize", $this->Section)));
    }


    /**
     * @param array $thisgroupp
     */
    public function setSections(array $data): void
    {
        foreach ($data as $value) {

                $this->Section[] = array('code' => $value['Ref_Key'], 'name' => $value['Description']);


        }

    }

    /**
     * @return array
     */
    public function getGroupSection(): array
    {
        return array_unique($this->GrouppSection);
    }

    /**
     * @param array $thisgroupp
     */
    public function setGroupSection(array $data): void
    {
        foreach ($data as $value) {

                $this->GrouppSection['cod'][] = $value['Ref_Key'];
                $this->GrouppSection['name'][] = $value['Description'];

        }
    }


}