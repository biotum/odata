<?php

if (!class_exists('FinishedProductFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductSectionFormatterInterface.php');
}
if (!class_exists('FinishedProductFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductSectionFormatter.php');
}

class FinishedProductRequest
{
    private $Product = [];
    private $data;
    private $ID;


    /**
     * @param array $data
     */
    public function setProduct(array $data): void
    {
        foreach ($data as $value) {
            if (strcmp($value['Parent_Key'], 00000000 - 0000 - 0000 - 0000 - 000000000000)!=35) {
                $this->Product[] = ['parentCod' => $value['Parent_Key'], 'cod' => $value['Ref_Key'], 'name' => $value['Description']];
            }
        }

    }

    /**
     * @return array
     */
    public function getProduct(): array
    {
        return $this->Product;
    }


}