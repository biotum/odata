<?php


if (!class_exists('Odatacfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/Odatacfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('ApiErpOdataClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ApiErp/ApiErpOdataClient.php');
}
if (!class_exists('FinishedProductSectionRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/FinishedProductSectionRequest.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/FinishedProductRequest.php');
}
if (!class_exists('FinishedProductFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductSectionFormatterInterface.php');
}
if (!class_exists('FinishedProductFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductSectionFormatter.php');
}
if (!class_exists('FinishedProductFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductFormatter.php');
}
if (!class_exists('Service')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/Service.php');
}
class Service
{
    protected $cfg;
    protected $client;
    protected $bitrixclient;
    protected $featchrequest;
    protected $valuesearch;
    protected $featchProductrequest;
    protected $listUpdate = [];
    protected $listProductUpdate = [];
    protected $listSectionProduct = [];
    protected $productlist=[];
    protected $isProduct=[];
    protected $isSection=[];


    /**
     * ProductService constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->cfg = new Odatacfg();
        $this->client = new ApiErpOdataClient();
        $this->bitrixclient = new ApiBitirxClient();
        $this->featchrequest = new FinishedProductSectionRequest();
        $this->featchProductrequest = new FinishedProductRequest();

    }

    /**
     * @return mixed
     */
    protected function getSectionlist($field=[])
    {

        if (empty($this->isSection)) {
            if ($field) {
                $data = ["auth" => $_REQUEST['AUTH_ID'], 'select' => $field];

            } else {
                $data = FinishedProductSectionFormatter::FormatlistSection();

            }

            foreach ($this->bitrixclient->getlistApiClient($data, 'crm.productsection.list') as $res) {
                $this->isSection[] = $res['result'];
            }
            $this->isSection = call_user_func_array('array_merge', $this->isSection);
        }
        return  $this->isSection;
    }


    /**
     * @param array $field
     * @return array|mixed
     */
    protected function getProductlist($field=[])
    {

        if (empty($this->isProduct)) {
            if ($field) {
                $data = ["auth" => $_REQUEST['AUTH_ID'], 'select' => $field];

            } else {
                $data = FinishedProductFormatter::FormatlistProduct();

            }

            foreach ($this->bitrixclient->getlistApiClient($data, 'crm.product.list') as $res) {
                $this->isProduct[] = $res['result'];
            }
            $this->isProduct = call_user_func_array('array_merge', $this->isProduct);
        }
     return  $this->isProduct;
    }

    /**
     * @param $products
     * @param $field
     * @param $value
     * @return bool
     */
    protected function inFind($array,$value)
    {
        $this->valuesearch=$value;
        $result=array_filter($array,function ($x){
            return $x['XML_ID']===$this->valuesearch;
        });
        if(!empty($result))
            return true;

    }
}