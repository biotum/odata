<?php


if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('FinishedProductSectionRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/FinishedProductSectionRequest.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/FinishedProductRequest.php');
}
if (!class_exists('FinishedProductFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductFormatterInterface.php');
}
if (!class_exists('FinishedProductFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductFormatter.php');
}
if (!class_exists('Service')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/Service.php');
}
class ProductService extends Service
{


    /**
     *
     */
    public function SaveProduct()
    {

        try {
         $this->ProductUpdate($this->client->RestApiClient($this->cfg::PRODUCT,[]));
            $this->client->close();

        } catch (Exception $exception) {

            var_dump($exception);
        }
    }

    /**
     * @param $response
     */
    private function ProductUpdate($response)
    {

        $this->featchProductrequest->setProduct($response['value']);

        foreach (FinishedProductFormatter::Format($this->featchProductrequest) as $value) {
            $this->listSectionProduct[] = $value['fields'];
        }

      foreach (FinishedProductFormatter::FormatSectionProduct($this->setProductSectionlist()) as $product) {


              if (!$this->isProductInsertlist($product['fields']['XML_ID'])) {


            $this->bitrixclient->ApiClient($product, 'crm.product.add');

              } else {
                  $this->listProductUpdate[] = ['ID' => '', 'XML_ID' => $product['fields']['XML_ID'], 'NAME' => $product['fields']['NAME']];
             }


      }
          if(!empty($this->listProductUpdate)) {
              foreach ($this->SectionUpdatelist() as $item => $value) {
                  $this->bitrixclient->ApiClient(FinishedProductFormatter::FormatUpdate($value['ID'],$value['NAME']), 'crm.product.update');
              }
          }



     $this->client->close();

    }

    /**
     * @return array
     */
    private function setProductSectionlist()
    {
        foreach ($this->getSectionlist( ["*"]) as $elm) {

            foreach ($this->listSectionProduct as $item => $grp) {

                if ($elm['XML_ID'] === $grp['XML_ID']) {

                    $this->listSectionProduct[$item]['SECTION_ID'] = $elm['ID'];

                }

            }


        }

        return $this->listSectionProduct;
    }
    /**
     * @param $isSection
     * @return bool
     */
    private function isProductInsertlist($value){
       return $this->inFind($this->getProductlist(),$value);
    }

    /**
     * @return array
     */
    private function SectionUpdatelist(){

        foreach( $this->isProduct as $elm) {

            foreach($this->listProductUpdate as $item=>$grp) {

                if ($elm['XML_ID'] === $grp['XML_ID']) {

                    $this->listProductUpdate [$item]['ID']=$elm['ID'];
                }

            }

        }

        return $this->listProductUpdate;
    }



}