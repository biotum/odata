<?php

if (!class_exists('Cfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}

class ApiBitirxClient extends CurlClient
{

        private $next;
        private $list=array();





        private function getUrl($metod){
            return 'https://'.cfg::URL_REST_BITRIX_DOMEIN.'/rest/92/vnvwf11qrwi7i00v/'.$metod;
        }

         public function ApiClient($data,$metod){

             $this->configure($this->getUrl($metod),$data,'POST');
             return  $this->execute();

         }

        public function getlistApiClient($data,$metod){
            $arrResults=[];
            $this->next=$this->NextResult($data,$metod);

           if($this->next>=1) {
                for ($i =0; $i <= $this->next-1; $i++) {
                    $data['start'] =$i * 50;
                    $arrResults[$i]= json_decode($this->ApiClient($data, $metod), true);

               }

         }else {

              $arrResults[0] = json_decode($this->ApiClient($data, $metod), true);
           }

            return   $arrResults;
        }

        private function NextResult($data,$metod){
            $resdecode=json_decode($this->ApiClient($data,$metod), true);
          return ceil(( $resdecode['total']/50));
        }





}