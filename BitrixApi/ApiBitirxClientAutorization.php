<?php

if (!class_exists('Cfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}

class ApiBitirxClientAutorization extends CurlClient
{

        private $next;
        private $list=array();





        private function getAutorizationUrl(){
            return 'https://'.cfg::URL_REST_BITRIX_DOMEIN.'/oauth/authorize/';
        }

         public function ApiClientAutorization(){

             $this->configure('https://'.cfg::URL_REST_BITRIX_DOMEIN.'/',[],'GET');

             $l = '';
             if(preg_match('#Location: (.*)#', $res, $r)) {
                 $l = trim($r[1]);
             }
             $res=$this->execute();
             preg_match('#name="backurl" value="(.*)"#', $res, $math);


             $post=[
                 'AUTH_FORM' => 'Y',
                 'TYPE' => 'AUTH',
                 'backurl' =>$math[1],
                 'USER_LOGIN' => cfg::LOGIN,
                 'USER_PASSWORD' => cfg::PASSWORD,
                 'USER_REMEMBER' => 'Y'
             ];

             $this->configure('https://'.cfg::URL_REST_BITRIX_DOMEIN.'/auth/',$post,'POST');
            $res=$this->execute();
             $l = '';
             if(preg_match('#Location: (.*)#', $res, $r)) {
                 $l = trim($r[1]);
             }


             $data=[
                 'response_type'=>'code',
                 'client_id'=>cfg::CODE
             ];

             $this->configure($this->getAutorizationUrl(),$data,'POST');

             $res=$this->execute();

 $this->close();


             print_r($res);
           //  $this->close();

             return '';


         }







}