<?php


class Odatacfg
{
const URL_REST_1C='http://10.97.21.10/RestApi/odata/standard.odata';

//rest методы 1с

const CLIENTS='/Catalog_Контрагенты';
const PARTNERS='/Catalog_Партнеры';
const ADDRESSSTORE='/Catalog_АдресаДоставки';
const BANKSACCOUNT='/Catalog_БанковскиеСчетаКонтрагентов';
const BANKCATALOG='/Catalog_%D0%9A%D0%BB%D0%B0%D1%81%D1%81%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%82%D0%BE%D1%80%D0%91%D0%B0%D0%BD%D0%BA%D0%BE%D0%B2';
//заявки клиентов
    const CUSTOMER='/Catalog_ЗаявкаКлиента';
    //статусы заявок клиентов
    const STATUSCUSTOMER='/Catalog_ЗаявкаКлиента?$select=Оплачено,ПроизводствоБезОплаты&$format=json';

//выбор секции продукции
const CATALOGPRODUCT='/Catalog_%D0%9D%D0%BE%D0%BC%D0%B5%D0%BD%D0%BA%D0%BB%D0%B0%D1%82%D1%83%D1%80%D0%B0?$select=Ref_Key,Description&$filter=Parent_Key%20eq(guid%27d939a88b-69e6-11e2-9e19-001e5848397d%27)&$format=json';

//вывод  продукции
const PRODUCT='/Catalog_%D0%9D%D0%BE%D0%BC%D0%B5%D0%BD%D0%BA%D0%BB%D0%B0%D1%82%D1%83%D1%80%D0%B0?$select=Parent_Key,Ref_Key,Description&$format=json';


//rest odata методы 1с
const ADDRESS_CLIENTS_DELIVERY='Address_clients_delivery';
const FINISED_PRODUCT='Finished_products';
const CLIENTSBANKS='Clients_banks';
const CLIENTSACCOUNTS='Clients_accounts';

//список событий и методы одата
const EVENTS=[
    'ONCRMDEALADD'=>['url'=>self::CUSTOMER,'metod'=>'POST'],
    'ONCRMDEALUPDATE'=>['url'=>self::CUSTOMER,'metod'=>'PATCH'],
    'ONCRMCOMPANYADD'=>['url'=>self::CLIENTS,'metod'=>'POST','url2'=>self::PARTNERS],
    'ONCRMCOMPANYUPDATE'=>['url'=>self::CLIENTS,'metod'=>'PATCH','url2'=>self::PARTNERS],
    'PARTNERS'=>['url'=>self::PARTNERS,'metod'=>'PATCH'],
    'ONCRMBANKDETAILADD'=>['url'=>self::BANKSACCOUNT,'metod'=>'POST'],
    'ONCRMBANKDETAILUPDATE'=>['url'=>self::BANKSACCOUNT,'metod'=>'PATCH'],
];

}