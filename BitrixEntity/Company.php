<?php

if (!class_exists('Product')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Product.php');
}
if (!class_exists('Store')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Store.php');
}
if (!class_exists('Requisite')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Requisite.php');
}
if (!class_exists('AddressRequisite')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/AddressRequisite.php');
}
if (!class_exists('DeliveryProduct')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/DeliveryProduct.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushDealRequest.php');
}
if (!class_exists('EntityInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/EntityInterface.php');
}
class Company implements EntityInterface
{

    private $bitrixclient;
    private $requisite;
    private $addressrequisite;

    public function __construct()
    {
        $this->bitrixclient = new ApiBitirxClient();
        $this->requisite=new Requisite();
        $this->addressrequisite=new AddressRequisite();
    }

    private function toArray($request){
        $res=json_decode($request,true);
        return $res['result'];
    }

    public function getPull($id){

     return $this->toArray($this->bitrixclient->ApiClient(['ID'=>$id], 'crm.company.get'));
    }



    /**
     * @param $id
     * @return array
     */
    public function getEntity($id){
        $request=new PushDealRequest();
        $request->setClient($this->getPull($id)?$this->getPull($id):[]);
        $request->setRequisite($this->requisite->getPull($id));
        $request->setAddressrequisite($this->addressrequisite->getPull($this->requisite->getPullID($id)));
        $request->getAddressrequisite();
        $request->getRequisite();
        $request->getClient();
        return $request;
    }

    /**
     * @param $gud
     * @param $entity_id
     */
    public function setReturnGuid($gud, $entity_id){
        $this->bitrixclient->ApiClient(['ID'=>$entity_id,'fields'=>['ORIGIN_ID'=>$gud]], 'crm.company.update');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getReturnGuid($id){
        $res=json_decode($this->bitrixclient->ApiClient(['id'=>$id], 'crm.company.get'),true);
        return $res['result']['ORIGIN_ID'];
    }

}