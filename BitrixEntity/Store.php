<?php

if (!class_exists('Product')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Product.php');
}
if (!class_exists('Store')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Store.php');
}
if (!class_exists('DeliveryProduct')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/DeliveryProduct.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushDealRequest.php');
}
if (!class_exists('EntityInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/EntityInterface.php');
}
class Store implements EntityInterface
{

    private $bitrixclient;

    public function __construct()
    {
        $this->bitrixclient = new ApiBitirxClient();
    }

    private function toArray($request){
        $res=json_decode($request,true);
        return $res['result'];
    }

    /**
     * @param $delivery
     * @return array
     */
    public function getStores($delivery){
        $result=[];
     foreach ($delivery as $store){

         $result[]= $this->toArray($this->bitrixclient->ApiClient(['filter'=>['ID'=>$store]], 'crm.store.list'));
     }

     return $result;
    }

    private function toArrays($request){
        $res=json_decode($request,true);
        return $res['result'][0];
    }

    public function getStore($id){

        return $this->toArrays($this->bitrixclient->ApiClient(['ID'=>$id], 'crm.store.get'));
    }


    /**
     * @param $id
     * @return array
     */
    public function getEntity($id){
        $store=$this->getStore($id);
        $request=new PushDealRequest();
        $request->setStore($store);
        $request->setONWERKEY($this->getReturnOnwerKey($id));

        return $request;
    }

    /**
     * @param $gud
     * @param $entity_id
     */
    public function setReturnGuid($gud, $entity_id){
        $this->bitrixclient->ApiClient(['ID'=>$entity_id,'fields'=>['XML_ID'=>$gud]], 'crm.store.update');
    }

    public function getReturnOnwerKey($id){
        $dbCompany = \Bitrix\Crm\CompanyTable::getList(array('select'=>['ID'],
            'filter' => array(
                'UF_CRM_COMPANY_TEST' => [$id]
            )
        ));
        $company = $dbCompany->fetch();
        $res=json_decode($this->bitrixclient->ApiClient(['id'=>$company['ID']], 'crm.company.get'),true);

        return $res['result']['ORIGIN_ID'];
    }

    public function getReturnGuid($id)
    {
        $res = json_decode($this->bitrixclient->ApiClient(['id' => $id], 'crm.store.get'), true);

        return $res['result'][0][0]['XML_ID'];
    }

    public function getAddress($id){


        foreach ($id as $item=>$value){
            $res[$item]=$this->toArray($this->bitrixclient->ApiClient(
                ['filter'=>['ID'=>$id],
                'select'=>['XML_ID']],
            'crm.store.list'));
        }
        return $res['ADDRESS'];
    }

}