<?php

if (!class_exists('Product')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Product.php');
}
if (!class_exists('Store')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Store.php');
}
if (!class_exists('DeliveryProduct')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/DeliveryProduct.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushDealRequest.php');
}
if (!class_exists('EntityInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/EntityInterface.php');
}
class AddressRequisite implements EntityInterface
{

    private $bitrixclient;

    public function __construct()
    {
        $this->bitrixclient = new ApiBitirxClient();
    }

    private function toArray($request){
        $res=json_decode($request,true);
        return $res['result'][0];
    }
    private function toArrays($request){
        $res=json_decode($request,true);
        return $res['result'];
    }

    public function getPull($id){

        return $this->toArrays($this->bitrixclient->ApiClient([
                                    'select'=>['*'],
                                    'filter'=>['ENTITY_ID'=>$id,'ENTITY_TYPE_ID'=>8]
                                 ],'crm.address.list')
                               );
    }


    /**
     * @param $id
     * @return array
     */
    public function getEntity($id){
        $request=new PushDealRequest();

         // $request->setAddressrequisite($this->getAddressRequisite($id));
          $request->getAddressrequisite();
        return $request;
    }
    /**
     * @param $gud
     * @param $entity_id
     */
    public function setReturnGuid($gud, $entity_id){
        $this->bitrixclient->ApiClient(['ID'=>$entity_id,'fields'=>['ORIGIN_ID'=>$gud]], 'crm.company.update');
    }

    public function getReturnGuid($id){
        $res=json_decode($this->bitrixclient->ApiClient(['id'=>$id], 'crm.company.get'),true);
        return $res['result']['ORIGIN_ID'];
    }

}