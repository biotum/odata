<?php

if (!class_exists('Events')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Events/Events.php');
}
if (!class_exists('Deal')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Deal.php');
}
if (!class_exists('Partners')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Partners.php');
}
interface EntityInterface
{

    public function getEntity($id);
    public function setReturnGuid($gud, $entity_id);
    public function getReturnGuid($id);

}