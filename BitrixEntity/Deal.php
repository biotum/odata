<?php
if (!class_exists('Company')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Company.php');
}
if (!class_exists('Product')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Product.php');
}
if (!class_exists('Store')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Store.php');
}
if (!class_exists('DeliveryProduct')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/DeliveryProduct.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushDealRequest.php');
}
if (!class_exists('EntityInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/EntityInterface.php');
}
class Deal implements EntityInterface
{
    private $bitrixclient;
    private $company;
    private $product;
    private $deliveryProduct;
    private $store;

    /**
     * Deal constructor.
     * @param ApiBitirxClient $apiBitirxClient
     * @param Company $company
     * @param Product $product
     * @param DeliveryProduct $deliveryProduct
     */
    public function __construct()
    {
        $this->company=new Company();
        $this->product=new Product();
        $this->deliveryProduct=new DeliveryProduct();
        $this->bitrixclient =new ApiBitirxClient();
        $this->store=new Store();
    }

    /**
     * @param $request
     * @return mixed
     */
    private function toArray($request){
        $res=json_decode($request,true);
        return $res['result'][0];
    }
    /**
     * @param $request
     * @return mixed
     */
    private function toGetArray($request){
        $res=json_decode($request,true);
        return $res['result'];
    }

    /**
     * @param $id
     * @return array
     */
    public function getEntity($id){
        $request=new PushDealRequest();
       $deal=$this->getDeal($id);
        $request->setDeal($deal);
        $request->setClient($this->company->getCompany($deal['COMPANY_ID'])?$this->company->getCompany($deal['COMPANY_ID']):[]);
        $request->setConsignee( $this->company->getCompany($this->deliveryProduct->getDealCompanyId($id))?$this->company->getCompany($this->deliveryProduct->getDealCompanyId($id)):[]);
        $request->setProduct($this->product->getProduct($this->getDealProductID($id))?$this->product->getProduct($this->getDealProductID($id)):[]);
        $request->setDeliveryproduct( $this->deliveryProduct->getDealDeliveryProduct($id));
        $request->setDealproduct($this->getDealProduct($id)?$this->getDealProduct($id):[]);
        $request->setStore( $this->store->getStores($this->deliveryProduct->getDealStoreID($id)));
        $request->setAddressdelivery($this->store->getAddress( $this->deliveryProduct->getDealStoreID($id)));
        $request->getPush();

      //  $this->bitrixclient->ApiClient(['POST_TITLE'=>'setConsignee','POST_MESSAGE'=> json_encode($this->company->getCompany($this->deliveryProduct->getDealCompanyId($id))),'DEST'=>['U92']], 'log.blogpost.add');

        return $request;
    }


    /**
     * @param $delivery
     * @return array
     */
    public function getDeal($id){

      return  $this->toArray($this->bitrixclient->ApiClient(['filter'=>['ID'=>$id]], 'crm.deal.list'));

        }


    public function getDealProductID($deal_id){
        $productDeal=$this->toArray(
            $this->bitrixclient->ApiClient(['ID'=>$deal_id],
            'crm.deal.productrows.get'));
        return $productDeal['PRODUCT_ID'];

    }
    public function getDealProduct($deal_id){
        $productDeal=$this->toArray(
            $this->bitrixclient->ApiClient(['ID'=>$deal_id],
                'crm.deal.productrows.get'));
        return $productDeal;

    }

    /**
     * @param $gud
     * @param $entity_id
     */
    public function setReturnGuid($gud, $entity_id){
        $this->bitrixclient->ApiClient(['ID'=>$entity_id,'fields'=>['ORIGIN_ID'=>$gud]], 'crm.deal.update');
    }

    public function getReturnGuid($id){
        $res=$this->toGetArray($this->bitrixclient->ApiClient(['id'=>$id], 'crm.deal.get'));
      return $res['ORIGIN_ID'];
    }
}