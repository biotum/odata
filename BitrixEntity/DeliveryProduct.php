<?php


class DeliveryProduct
{
    private $bitrixclient;

    public function __construct()
    {
        $this->bitrixclient = new ApiBitirxClient();
    }

    private function toArray($request){
        $result=json_decode($request,true);
        return  $result['result'][0];

    }
    private function toArrays($request){
        $result=json_decode($request,true);
        return  $result['result'];

    }

    public function getDeliveryProduct($id){

        return $this->toArray($this->bitrixclient->ApiClient(['filter'=>['ID'=>$id]], 'crm.deliveryproduct.list'));
    }

    public function getDealDeliveryProduct($id){

        return $this->toArrays($this->bitrixclient->ApiClient(['filter'=>['DEAL_ID'=>$id]], 'crm.deliveryproduct.list'));
    }

    public function getDealCompanyID($id){
        $res=$this->toArray($this->bitrixclient->ApiClient(['filter'=>['DEAL_ID'=>$id],'select'=>['COMPANY_ID']], 'crm.deliveryproduct.list'));
        return $res['COMPANY_ID'];
    }

    public function getDealStoreID($id){
        $res=$this->toArrays($this->bitrixclient->ApiClient(['filter'=>['DEAL_ID'=>$id],'select'=>['STORE_ID']], 'crm.deliveryproduct.list'));

        return $res;
    }
}