<?php


class Product
{

    private $bitrixclient;

    public function __construct()
    {
        $this->bitrixclient = new ApiBitirxClient();
    }

    private function toArray($request){
        $res=json_decode($request,true);
        return $res['result'];
    }

    public function getProduct($id){

        return $this->toArray($this->bitrixclient->ApiClient(['ID'=>$id], 'crm.product.get'));
    }

}