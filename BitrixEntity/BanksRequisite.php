<?php

if (!class_exists('Product')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Product.php');
}
if (!class_exists('Store')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Store.php');
}
if (!class_exists('DeliveryProduct')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/DeliveryProduct.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushDealRequest.php');
}
if (!class_exists('EntityInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/EntityInterface.php');
}

class BanksRequisite implements EntityInterface
{

    private $bitrixclient;
    private $banks;
    private $requisite;
    private $company;
    public function __construct()
    {
        $this->bitrixclient = new ApiBitirxClient();
        $this->requisite=new Requisite();
        $this->company=new Company();
        $this->banks= new BanksCatalog();
    }

    /**
     * @param $request
     * @return mixed
     */
    private function toArray($request)
    {
        $res = json_decode($request, true);
        return $res['result'][0];
    }

    /**
     * @param $request
     * @return mixed
     */
    private function toArrays($request)
    {
        $res = json_decode($request, true);
        return $res['result'];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPull($id)
    {

        return $this->toArrays($this->bitrixclient->ApiClient(['id' => $id], 'crm.requisite.bankdetail.get'));
    }


    /**
     * @param $id
     * @return array
     */
    public function getEntity($id)
    {
        $request = new PushDealRequest();
        $request->setBanksrequisite($this->getOdataBanks($this->getPull($id)));
        $request->getBanksrequisite();

        return $request;
    }

    private function getOdataBanks($array){

        $result=$this->banks->getFetch($array['RQ_BIK']);
      return array_merge($this->getOnwer($array),$result)?array_merge($this->getOnwer($array),$result):[];
    }

    private function getOnwer($array)
    {

    if(!$array['ORIGINATOR_ID'])
       {
            $result = $this->requisite->getRequisite($array['ENTITY_ID']);
            $array['ORIGINATOR_ID'] = $this->company->getReturnGuid($result['ENTITY_ID']);

      }
        return  $array;
    }
    /**
     * @param $guid
     * @param $entity_id
     */
    public function setReturnGuid($guid, $entity_id)
    {
        $this->bitrixclient->ApiClient(['ID' => $entity_id, 'fields' => ['XML_ID' => $guid]], 'crm.requisite.bankdetail.update');
    }

    public function getReturnGuid($id)
    {
        $res = json_decode($this->bitrixclient->ApiClient(['id' => $id], 'crm.requisite.bankdetail.get'), true);
        return $res['result']['XML_ID'];
    }

}