<?php

if (!class_exists('Cfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/Odatacfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('Odatafg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/Odatacfg.php');
}
class ApiErpOdataClient extends CurlClient
{

         public function RestApiClient($metod,$param=[]){

             $this->configure(Odatacfg::URL_REST_1C.iconv('utf-8','windows-1251',$metod),$param,true);
             $result=json_decode($this->execute(), true);
             $this->close();
             return $result;
         }


        public function RestPushApiClient($metod,$param,$typerequest =''){

            $this->configure(Odatacfg::URL_REST_1C.iconv('utf-8','windows-1251',$metod),$param,$typerequest,true);
            $result=json_decode($this->execute(), true);
          //  $this->close();
            return $result;
        }

        public function convertURL($Odatametod,$guid){

             return $guid?$Odatametod.'(guid%20%27'.$guid.'%27)?$format=json':$Odatametod.'?$format=json';
        }



}