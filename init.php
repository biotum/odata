<?php
if (!class_exists('ClientsService')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsService.php');
}
if (!class_exists('AddressRequisiteFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/AddressRequisiteFormatter.php');
}
if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
}
if (!class_exists('ClientsBankAccountFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsBankAccountFormatter.php');
}
if (!class_exists('RequisiteFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/RequisiteFormatter.php');
}
if (!class_exists('PushDealFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushDealFormatter.php');
}
if (!class_exists('PushService')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushService.php');
}
if (!class_exists('Deal')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Deal.php');
}

function custom_mail($to,$subject,$body,$headers) {
    $f=fopen($_SERVER["DOCUMENT_ROOT"]."/maillog.txt", "a+");
    fwrite($f, print_r(array('TO' => $to, 'SUBJECT' => $subject, 'BODY' => $body, 'HEADERS' => $headers),1)."\n========\n");
    fclose($f);
    return mail($to,$subject,$body,$headers);
}

function testAgent()
{
    foreach ((new Events())->pushEvents() as $entity) {

        switch ((new Events())->entityEvent($entity['EVENT_NAME'])) {
            case "COMPANY":
                (new PushService(new PushCompanyFormatter(), new Company()))->event();
                break;
            case "DEAL":
                (new PushService(new PushDealFormatter(), new Deal()))->event();
                break;
            case "пирог":
                echo "i это пирог";
                break;
        }
    }

 // mail('web@biotum.ru', 'Агент', 'Агент');
    return "testAgent();";
}

function SaveClients()
{
    (new ClientsService(new ClientsFormatter()))->SaveClients();

    return "SaveClients();";
}

function SaveAddressRequisite()
{

    (new ClientsService(new AddressRequisiteFormatter()))->SaveAddressRequisite();

    return "SaveAddressRequisite();";

}
function SaveRequisite()
{

    (new ClientsService(new RequisiteFormatter()))->SaveRequisite();

    return "SaveRequisite();";

}

function SaveBanksRequisite(){

    (new ClientsService(new ClientsBankAccountFormatter()))->SaveClientsBankAccount();
    return "SaveBanksRequisite();";
}


?>
