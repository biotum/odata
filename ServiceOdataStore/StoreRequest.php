<?php

if (!class_exists('ClientsFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
}

class StoreRequest
{
    private $Store = [];


/**
 * @param array $data
 */
    public function setStore(array $data): void
    {
        foreach ($data as $value) {

                $this->Store[] = [
                    'Ref_Key' => $value['Ref_Key'],
                    'DataVersion' =>'AAAAAAAbdcI=',
                    'DeletionMark'=>false,
                   'Owner_Key'=>$value['Owner_Key'],
                   'Code'=>$value['Owner_Key'],
                    'Description'=>$value['Description'],
                    'Адрес'=>$value['Адрес'],
                    'Predefined'=>false,
                    'PredefinedDataName'=>'',
                    'Owner@navigationLinkUrl'=>'Catalog_АдресаДоставки(guid"'.$value['Owner_Key'].'")/Owner'


                ];

        }

    }


    /*
     * /
     */

    /**
     * @return array
     */
    public function getStore():array
    {
        return $this->Store;
    }

}