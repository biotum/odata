<?php


if (!class_exists('OdataCfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/Odatacfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ApiErp/ApiErpOdataClient.php');
}

if (!class_exists('StoreRequest.php')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataStore/StoreRequest.php');
}
if (!class_exists('StoreFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/StoreFormatterInterface.php');
}
if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/StoreFormatter.php');
}
if (!class_exists('CompanyStoreFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/CompanyStoreFormatter.php');
}
class BaseOdataStore
{
    protected $odatacfg;
    protected $client;
    protected $bitrixclient;
    protected $valuesearch;
    protected $featchStorerequest;
    protected $listStoresUpdate = [];
    protected $listStores = [];
    protected $isStore=[];
    protected $Formatter;
    protected $isClients=[];

    /**
     * BaseOdataService constructor.
     * @param FormatterInterface $Formatter
     */
    public function __construct(StoreFormatterInterface $clientsFormatter){
        $this->Formatter=$clientsFormatter;
        $this->odatacfg = new OdataCfg();
        $this->client = new ApiErpOdataClient();
        $this->bitrixclient = new ApiBitirxClient();
        $this->featchStorerequest = new StoreRequest();

    }

    /**
     * @param array $field
     * @return array|mixed
     */
    protected function getStoreslist($field=[])
    {

        if (empty($this->isStore)) {
            if ($field) {
                $data = ["auth" => $_REQUEST['AUTH_ID'], 'select' => $field];

            } else {
                $data = $this->Formatter->Formatlist();

            }
            foreach ($this->bitrixclient->getlistApiClient($data, 'crm.store.list') as $item=>$res) {
               // print_r($item);
                $this->isStore[]=$res['result'];
            }
            $this->isStore = call_user_func_array('array_merge', $this->isStore);
        }

     return  $this->isStore;
    }
    /**
     * @param array $field
     * @return array|mixed
     */
    protected function getClientslist($field=[])
    {

        if (empty($this->isClients)) {
            if ($field) {
                $data = ["auth" => $_REQUEST['AUTH_ID'], 'select' => $field];

            } else {
                $data = $this->Formatter->Formatlist();

            }

            foreach ($this->bitrixclient->getlistApiClient($data, 'crm.company.list') as $res) {
                $this->isClients[] = $res['result'];
            }
            $this->isClients = call_user_func_array('array_merge', $this->isClients);
        }
        return  $this->isClients;
    }

    /**
     * @param $products
     * @param $field
     * @param $value
     * @return bool
     */
    protected function inFind($array,$field,$values=null)
    {
        foreach ($array as $item=>$value)
        {
            $id=array_search($values,array_column($array[$item], $field));
            if ((boolean)$id or $id === 0) {
                //    $fields = array_column($array[$item], 'ID');
               //     print_r('++' . $fields[$id] . '++++');
                    return true;
         }

        }

    return false;

    }

    /**
     * @param $products
     * @param $field
     * @param $value
     * @return bool
     */
    protected function isFindID($array,$field,$values=null)
    {
        foreach ($array as $item=>$value)
        {
            $id=array_search($values,array_column($array[$item], $field));
            if ((boolean)$id or $id === 0) {
                    $fields = array_column($array[$item], 'ID');
                  //   print_r('++' . $fields[$id] . '++++');
                return $fields[$id];
            }

        }

        return false;

    }

    /**
     * @param $products
     * @param $field
     * @param $value
     * @return bool
     */
    protected function isFindField($array,$field,$values=null,$result_field)
    {


        $level_array=call_user_func_array('array_merge', $array);
        if($level_array[0]) {

            foreach ($array as $item => $value) {
                $id = array_search($values, array_column($array[$item], $field));

                if ((boolean)$id or $id === 0) {
                    $fields = array_column($array[$item], $result_field);
                    //   print_r('++' . $fields[$id] . '++++');
                    return $fields[$id];
                }

            }
        }else{
            $ids=array_search($values,array_column($array, $field));
            $fields = array_column($array, $result_field);
           return $fields[$ids];
        }
        return false;

    }

    protected function array_unique_key($array, $key) {
        $tmp = $key_array = array();
        $i = 0;

        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $tmp[$i] = $val;
            }
            $i++;
        }
        return $tmp;
    }
}