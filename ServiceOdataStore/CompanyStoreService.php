<?php



use Bitrix\Main\EventManager;
use Bitrix\Rest\Api\Events;
use Bitrix\Rest\EventTable;

if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}

if (!class_exists('ClientsRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

if (!class_exists('FormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
}

if (!class_exists('RequisiteFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/StoreFormatter.php');
}
if (!class_exists('AddressRequisiteFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/AddressRequisiteFormatter.php');
}
if (!class_exists('ClientsBankAccountFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsBankAccountFormatter.php');
}
if (!class_exists('BaseOdataStore')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataStore/BaseOdataStore.php');
}
//if (!class_exists('Events')) {
//    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Events/Events.php');
//}
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');


class CompanyStoreService extends BaseOdataStore
{


    private $requisite = [];


    /**
     *
     */
    protected function FetchCompanyStore()
    {
        $array = [];

        try {

            $response = $this->client->RestApiClient($this->odatacfg::ADDRESSSTORE,['$select'=>'Ref_Key,Owner_Key,Description','$format'=>'json']);
            $this->featchStorerequest->setStore($response['value']);
            $this->client->close();

        } catch (Exception $exception) {

            var_dump($exception);

        }

    }

    /**
     * @param $response
     */
    public function SaveCompanyStore()
    {
        $this->FetchCompanyStore();
        $this->getStoreslist();
        $this->getClientslist();
        $array=[];
        foreach ($this->Formatter->Format($this->featchStorerequest) as $value) {
                $insert[]=['client_id'=>$this->isFindField($this->isClients,'ORIGIN_ID',$value['fields']['ORDER_ID'],'ID'),
                    'store_id'=>$this->isFindField($this->isStore,'XML_ID',$value['fields']['XML_ID'],'ID'),
                ];
        }

        $result = array();
        foreach ($insert as $data) {
            $id = $data['client_id'];
            if (isset($result[$id])) {
                $result[$id][] =$data['store_id'];
            } else {
                $result[$id] = [$data['store_id']];
            }
        }
        //print_r($this->isClients);
       print_r($GLOBALS["USER_FIELD_MANAGER"]->GetUserFields ("CRM_COMPANY",'UF_CRM_1571299242'));
        foreach ($this->isClients as $values){
            print_r($GLOBALS["USER_FIELD_MANAGER"]->GetUserFields ("CRM_COMPANY",$values['ID'],'UF_CRM_1571299242'));
   //   $GLOBALS["USER_FIELD_MANAGER"]->Update("CRM_COMPANY", $values['ID'], Array("UF_CRM_COMPANY_TEST"=>$result[$values['ID']]));
        }

        $this->client->close();

    }



}