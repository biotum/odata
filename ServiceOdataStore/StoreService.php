<?php



use Bitrix\Main\EventManager;
use Bitrix\Rest\Api\Events;
use Bitrix\Rest\EventTable;

if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}

if (!class_exists('ClientsRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

if (!class_exists('FormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
}

if (!class_exists('RequisiteFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/StoreFormatter.php');
}
if (!class_exists('AddressRequisiteFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/AddressRequisiteFormatter.php');
}
if (!class_exists('ClientsBankAccountFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsBankAccountFormatter.php');
}
if (!class_exists('BaseOdataStore')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataStore/BaseOdataStore.php');
}
//if (!class_exists('Events')) {
//    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Events/Events.php');
//}
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');


class StoreService extends BaseOdataStore
{


    private $requisite = [];

    /**
     *
     */
    protected function FetchStore()
    {
        $array = [];

        try {

            $response = $this->client->RestApiClient($this->odatacfg::ADDRESSSTORE,['$format'=>'json']);
            $this->featchStorerequest->setStore($response['value']);
            $this->client->close();

        } catch (Exception $exception) {

            var_dump($exception);

        }

    }


    /**
     * @param $response
     */
    public function SaveStore()
    {
        $this->FetchStore();
        $this->getStoreslist();

        foreach ($this->Formatter->Format($this->featchStorerequest) as $value) {
            $this->listStores[] = $value['fields'];

        }
     // print_r($this->isClientsInsertlist(5680));
       // print_r(array_column($this->isStore[17], 'ID'));

     //   print_r( array_search('6293ac72-83ec-11e7-0696-005056891ac8',array_column($this->listStores, 'ORDER_ID')));
      foreach ($this->Formatter->Format($this->featchStorerequest) as $Store) {


       //  print_r( $this->inFind($this->isStore, '70ed68f6-c868-11e7-3b97-005056891ac8'));

      if (!$this->isClientsInsertlist($Store['fields']['ORDER_ID'])) {


  print_r(json_encode( $this->bitrixclient->ApiClient($Store, 'crm.store.add'),true));

        }
      else
          {

        $this->listStoresUpdate[] = $this->Formatter->FormatlistUpdate($Store);

          }
     }

       if (!empty($this->listStoresUpdate)) {

         foreach ($this->getClientsUpdatelist() as $item => $value) {

   print_r(json_encode($this->bitrixclient->ApiClient($this->Formatter->FormatUpdate($value), 'crm.store.update'),true));


          }

      }

        $this->client->close();

    }



    /**
     * @param $isSection
     * @return bool
     */
    private function isClientsInsertlist($value)
    {

        return $this->inFind($this->isStore, 'ORDER_ID',$value);
    }

    /**
     * @return array
     */
    private function getClientsUpdatelist()
    {

            foreach ($this->listStoresUpdate as $item => $grp) {
                $this->listStoresUpdate[$item]['ID'] =$this->isFindID($this->isStore, 'ORDER_ID',$grp['ORDER_ID']);
        }
        return $this->listStoresUpdate;
    }


}