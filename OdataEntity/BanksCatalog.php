<?php

if (!class_exists('Product')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Product.php');
}
if (!class_exists('Store')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/Store.php');
}
if (!class_exists('DeliveryProduct')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/DeliveryProduct.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushDealRequest.php');
}
if (!class_exists('EntityInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixEntity/EntityInterface.php');
}
class BanksCatalog
{

    private $client;
    private $odatacfg;

    public function __construct()
    {
        $this->odatacfg = new OdataCfg();
        $this->client = new ApiErpOdataClient();
    }


    /**
     * @param $bic
     * бик банка
     * @return mixed
     */
    public function getFetchGuid($bic){

        try {
            $response = $this->client->RestApiClient($this->odatacfg::BANKCATALOG.'?$select=Ref_Key&$filter=Code%20eq%20%27'.$bic.'%27&$format=json',[]);

           $this->client->close();

        } catch (Exception $exception) {

            return $exception;

        }
        return $response['value'][0]['Ref_Key'] ;
    }

    /**
     * @param $bic
     * бик банка
     * @return mixed
     */
    public function getFetch($bic){

        try {
            $response = $this->client->RestApiClient($this->odatacfg::BANKCATALOG.'?$filter=Code%20eq%20%27'.$bic.'%27&$format=json',[]);

            $this->client->close();

        } catch (Exception $exception) {

            return $exception;

        }
        return $response['value'][0] ;
    }


}