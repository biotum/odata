<?php
//if (!class_exists('Cfg')) {
//    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
//}
//if (!class_exists('CurlClient')) {
//    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
//}
//if (!class_exists('ApiClient')) {
//    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
//}
if (!class_exists('PushFormatterInterface.php')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushFormatterInterface.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

class PushBanksRequisiteFormatter implements PushFormatterInterface
{
    private $onwerKey;
    /**
     * @param PushDealRequest $clientRequest
     * @return array|mixed
     */
    public  function Format(PushDealRequest $clientRequest)
    {
        $bankrequisite=$clientRequest->getBanksrequisite();
        $result=[
                //"Ref_Key"=> "d2f119be-831f-11e7-0696-005056891ac8",
            "DataVersion"=> "AAAAAAAaHrE=",
            "DeletionMark"=>false,
            "Owner"=> $bankrequisite['ORIGINATOR_ID'],
            "Owner_Type"=> "StandardODATA.Catalog_Контрагенты",
            "Description"=>  $bankrequisite['RQ_ACC_NUM'].','.$bankrequisite['Description'],
            "НомерСчета"=> $bankrequisite['RQ_ACC_NUM'],
            "Банк_Key"=> $bankrequisite['Ref_Key'],
            "БанкДляРасчетов_Key"=> "00000000-0000-0000-0000-000000000000",
            "ТекстКорреспондента"=> "",
            "ТекстНазначения"=> "",
            "ВалютаДенежныхСредств_Key"=> "7a595fb0-0ccc-11e8-897d-704d7b7030ea",
            "БИКБанка"=> "",
            "РучноеИзменениеРеквизитовБанка"=> false,
            "НаименованиеБанка"=> "",
            "КоррСчетБанка"=> "",
            "ГородБанка"=> "",
            "АдресБанка"=> "",
            "ТелефоныБанка"=> "",
            "БИКБанкаДляРасчетов"=> "",
            "РучноеИзменениеРеквизитовБанкаДляРасчетов"=> false,
            "НаименованиеБанкаДляРасчетов"=> "",
            "КоррСчетБанкаДляРасчетов"=> "",
            "ГородБанкаДляРасчетов"=> "",
            "АдресБанкаДляРасчетов"=> "",
            "ТелефоныБанкаДляРасчетов"=> "",
            "СВИФТБанка"=> "",
            "СВИФТБанкаДляРасчетов"=> "",
            "ИностранныйБанк"=> false,
            "СчетВБанкеДляРасчетов"=> "",
            "Закрыт"=> false,
            "ОтдельныйСчетГОЗ"=> false,
            "ГосударственныйКонтракт_Key"=> "00000000-0000-0000-0000-000000000000",
            "ИННКорреспондента"=> "",
            "КППКорреспондента"=> "",
            "НаименованиеБанкаМеждународное"=> "",
            "ГородБанкаМеждународный"=> "",
            "АдресБанкаМеждународный"=> "",
            "НаименованиеБанкаДляРасчетовМеждународное"=> "",
            "ГородБанкаДляРасчетовМеждународный"=> "",
            "АдресБанкаДляРасчетовМеждународный"=> "",
            "СтранаБанка_Key"=> "00000000-0000-0000-0000-000000000000",
            "СтранаБанкаДляРасчетов_Key"=> "00000000-0000-0000-0000-000000000000",
            "Predefined"=> false,
            "PredefinedDataName"=> "",
            "Банк@navigationLinkUrl"=> 'Catalog_БанковскиеСчетаКонтрагентов(guid "'.$bankrequisite['ORIGINATOR_ID'].'")/Банк',
            "ВалютаДенежныхСредств@navigationLinkUrl"=> 'Catalog_БанковскиеСчетаКонтрагентов(guid "'.$bankrequisite['ORIGINATOR_ID'].'")/ВалютаДенежныхСредств'
        ];
        return $result;
    }



    public function setOnwerKey($key){
        $this->onwerKey=$key;
    }
    public function getOnwerKey(){
        return $this->onwerKey;
    }
}