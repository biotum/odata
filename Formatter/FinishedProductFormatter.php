<?php
if (!class_exists('Cfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('FinishedProductSectionFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductFormatterInterface.php');
}
if (!class_exists('FinishedProductSectionRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/FinishedProductRequest.php');
}

class FinishedProductFormatter implements FinishedProductFormatterInterface
{

    public static function Format(FinishedProductRequest $finishedProductRequest)
    {

        foreach ($finishedProductRequest->getProduct() as $product) {
            $result[] = [
                "auth" => $_REQUEST['AUTH_ID'],
                'fields' => [

                    'CATALOG_ID' => 27,
                    'NAME' => $product['name'],
                    'SECTION_ID' => '',
                    'XML_ID' => $product['parentCod'],
                    'ID_PROD' => $product['cod']
                ]
            ];
        }

        return $result;


    }

    public static function FormatSectionProduct($data)
    {
     $result = [];
        foreach ($data as $value) {
            if($value['SECTION_ID']) {
                $result[] = [
                    "auth" => $_REQUEST['AUTH_ID'],
                    'fields' => [
                        'ID' => $value['ID'],
                        'NAME' => $value['NAME'],
                        'SECTION_ID' => $value['SECTION_ID'],
                        'XML_ID' => $value['ID_PROD'],
                    ]
                ];
            }
        }
        return $result;


    }

    public static function FormatUpdate($id, $data)
    {

        $result = [
            "auth" => $_REQUEST['AUTH_ID'],
            'ID' => $id,
            'fields' => [
                'NAME' => $data,
            ]
        ];


        return $result;


    }

    public static function FormatlistProduct(){
        $result = ["auth" => $_REQUEST['AUTH_ID'], 'order'=>['ID'=>'ASC'],'select' => ["ID","XML_ID","NAME","SECTION_ID"]];
        return $result;
    }


}