<?php
if (!class_exists('Cfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('ClientsFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('ClientsRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

class AddressRequisiteFormatter implements FormatterInterface
{

    public  function Format(ClientsRequest $clientRequest)
    {

        foreach ($clientRequest->getClients() as $client) {

            $json_string = json_encode(simplexml_load_string($client['КонтактнаяИнформация'][2]['ЗначенияПолей'], 'SimpleXMLElement', LIBXML_NOCDATA));
            $result_array = json_decode($json_string, TRUE);
            //   print_r($result_array);

            $result[] = [
                "auth" => $_REQUEST['AUTH_ID'],
               'fields' => [
                   'XML_ID' => $client['Ref_Key'],
                   'TITLE'=>$client['Description'],
                   'TYPE_ID'=>1,
                   'ENTITY_TYPE_ID' =>4,
                   'ENTITY_ID'=>'',
                    'ADDRESS_1'=>$result_array['Состав']['Состав']['Улица'].','.$result_array['Состав']['Состав']['ДопАдрЭл'][1]['Номер']['@attributes']['Значение'],
                    'ADDRESS_2'=>$result_array['Состав']['Состав']['ДопАдрЭл'][2]['Номер']['@attributes']['Значение'],
                    'CITY'=>$result_array['Состав']['Состав']['Город']?$result_array['Состав']['Состав']['Город']:$client['КонтактнаяИнформация'][2]['Регион'],
                    'POSTAL_CODE'=>$result_array['Состав']['Состав']['ДопАдрЭл'][0]['@attributes']['Значение'],
                    'PROVINCE'=>$result_array['Состав']['Состав']['СубъектРФ'],
                    'COUNTRY'=>$result_array['Состав']['@attributes']['Страна'],
                    'COUNTRY_CODE'=>$result_array['Состав']['@attributes']['Страна']?643:'',
                    'ANCHOR_ID'=>'',
                    'ANCHOR_TYPE_ID'=>4

               ]
            ];
        }

        return $result;


    }

    public  function FormatlistUpdate($data)
    {
        $result = [];
        foreach ($data as $client) {
            $result[] = [
                "auth" => $_REQUEST['AUTH_ID'],
                'fields' => [
                    'TITLE' => $client['name'],
                    'SECTION_ID' =>$client['inn'],
                    'XML_ID' => $client['codOkpo'],
                    'ID_PROD' =>$client['fullname'],
                ]
            ];
        }
        return $result;

    }

    public  function FormaInsert($data)
    {

        $result = [
            "auth" => $_REQUEST['AUTH_ID'],
            'fields' =>[
                'TYPE_ID'=>1,
                'ENTITY_TYPE_ID' => 4,
                'ENTITY_ID'=>$data['ENTITY_ID'],
                'ADDRESS_1'=>$data['ADDRESS_1'],
                'ADDRESS_2'=>$data['ADDRESS_2'],
                'CITY'=>$data['CITY'],
                'POSTAL_CODE'=>$data['POSTAL_CODE'],
                'PROVINCE'=>$data['PROVINCE'],
                'COUNTRY'=>$data['COUNTRY'],
                'COUNTRY_CODE'=>$data['COUNTRY_CODE'],
                'ANCHOR_ID'=>$data['ANCHOR_ID'],
                'ANCHOR_TYPE_ID'=>4

            ]

        ];


        return $result;


    }

    public  function FormatUpdate($id,$data)
    {

        $result = [
            "auth" => $_REQUEST['AUTH_ID'],
            'ID'=>$id,
            'fields' => $data
        ];

        return $result;


    }

    public  function Formatlist(){
        $result = ["auth" => $_REQUEST['AUTH_ID'],'select' => ["*"]];
        return $result;
    }



}