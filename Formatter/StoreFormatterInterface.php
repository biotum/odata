<?php

if (!class_exists('StoreFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/StoreFormatter.php');
}
if (!class_exists('StoreRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataStore/StoreRequest.php');
}

interface StoreFormatterInterface
{

    /**
     * @return mixed
     */
    public  function Format(StoreRequest $clientsRequest);

    /**
     * @return mixed
     */
    public  function FormatlistUpdate($data);
    public  function FormatUpdate ($data);
    public  function Formatlist();
    public  function FormaInsert ($data);



}