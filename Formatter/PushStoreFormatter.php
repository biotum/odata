<?php

if (!class_exists('PushFormatterInterface.php')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushFormatterInterface.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

class PushStoreFormatter implements PushFormatterInterface
{
    private $onwerKey;

    /**
     * @param PushDealRequest $clientRequest
     * @return array|mixed
     */
    public  function Format(PushDealRequest $clientRequest)
    {
        $store=$clientRequest->getStore();

        return [
           // 'Ref_Key'=>$store[0]['XML_ID'],
            'DataVersion'=>'AAAAAAAbf2E=',
            'DeletionMark'=>false,
            'Owner_Key'=>$clientRequest->getONWERKEY()?$clientRequest->getONWERKEY():[],
           // 'Code'=>0,
            'Description'=>$store[0]['NAME'],
            'Адрес'=>$store[0]['ADDRESS'],
            'Predefined'=> false,
            'PredefinedDataName'=>'',
            'Owner@navigationLinkUrl'=>"Catalog_АдресаДоставки(guid'".$store[0]['XML_ID']."')/Owner"
        ];

            }


    public function setOnwerKey($key){
        $this->onwerKey=$key;
    }

    public function getOnwerKey(){
        return $this->onwerKey;
    }

}