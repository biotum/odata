<?php

if (!class_exists('FinishedProductFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductFormatter.php');
}
if (!class_exists('FinishedProductSectionRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/FinishedProductRequest.php');
}

interface FinishedProductFormatterInterface
{

    /**
     * @return mixed
     */
    public static function Format(FinishedProductRequest $finishedProductRequest);

    /**
     * @return mixed
     */
    public static function FormatSectionProduct($data);


    /**
     * @return mixed
     */
    public static function FormatlistProduct();

    public static function FormatUpdate ($id, $data);




}