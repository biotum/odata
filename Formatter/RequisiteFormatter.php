<?php
if (!class_exists('Cfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('FormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('ClientsRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

class RequisiteFormatter implements FormatterInterface
{

    public  function Format(ClientsRequest $clientRequest)
    {

        foreach ($clientRequest->getClients() as $client) {
            if($client['ЮридическоеФизическоеЛицо']=='ЮридическоеЛицо') {
              $type='Организация';
            }else{
                $type='ИП';
            }
            $result[] = [
                "auth" => $_REQUEST['AUTH_ID'],
                'fields' => [
                    'XML_ID' => $client['Ref_Key'],
                    'ENTITY_ID'=>'',
                    'ENTITY_TYPE_ID' => 4,
                    'NAME'=>$type,
                    'PRESET_ID'=>1,
                    'ACTIVE'=>"Y",
                    'RQ_COMPANY_FULL_NAME' =>$client['НаименованиеПолное']?$client['НаименованиеПолное'] :$client['Description'],
                    'RQ_COMPANY_NAME'=>$client['Description'],
                    'RQ_INN' => $client['ИНН'],
                    'RQ_KPP' =>$client['КПП'],
                    'RQ_OGRN'=>mb_strimwidth($client['РегистрационныйНомер'],0,13,''),
                    'RQ_OKPO'=>$client['КодПоОКПО'],
                ]
            ];
        }

        return $result;


    }

    public  function FormatlistUpdate($data)
    {
        $result = [];
        foreach ($data as $client) {
            $result[] = [
                "auth" => $_REQUEST['AUTH_ID'],
                'fields' => [
                    'TITLE' => $client['name'],
                    'SECTION_ID' =>$client['inn'],
                    'XML_ID' => $client['codOkpo'],
                    'ID_PROD' =>$client['fullname'],
                ]
            ];
        }
        return $result;

    }

    public  function FormaInsert($data)
    {

        $result = [
            "auth" => $_REQUEST['AUTH_ID'],
            'fields' =>$data
        ];


        return $result;


    }

    public  function FormatUpdate($id,$data)
    {

        $result = [
            "auth" => $_REQUEST['AUTH_ID'],
            'ID'=>$id,
            'fields' => $data
        ];

        return $result;


    }

    public  function Formatlist(){
        $result = ["auth" => $_REQUEST['AUTH_ID'], 'order'=>['ID'=>'ASC'],'select' => ["*"],'filter'=>['!XML_ID'=>false]];
        return $result;
    }



}