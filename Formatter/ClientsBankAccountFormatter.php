<?php
if (!class_exists('Cfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('FinishedProductSectionFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

class ClientsBankAccountFormatter implements FormatterInterface
{

    public  function Format(ClientsRequest $clientRequest)
    {

        foreach ($clientRequest->getClientsBank() as $client) {

                $result[] = [
                    "auth" => $_REQUEST['AUTH_ID'],
                    'fields' => [
                   'XML_ID' => $client['Ref_Key'],
                   'ENTITY_TYPE_ID' => 8,
                   'ENTITY_ID'=>'',
                    'NAME'=>'Реквизит банка',
                    'RQ_BANK_NAME'=>$client['bank_Description'],
                     'RQ_BANK_ADDR'=>$client['bank_Адрес'],
                     'RQ_ACC_NUM'=>$client['НомерСчета'],
                     'RQ_BIK'=>$client['bank_Code'],
                     'RQ_COR_ACC_NUM'=>$client['bank_КоррСчет'],
                     'RQ_SWIFT'=>$client['bank_СВИФТБИК'],
                      'ORIGINATOR_ID'=>$client['client_Ref_Key'],
                      'ACTIVE'=>'Y',
			          'SORT'=>100
                    ]
                ];
/*
 * ENTITY_ID	Идентификатор родительской сущности. Обязательное поле.	Да	Да
COUNTRY_ID	Идентификатор страны.	Да	Да
DATE_CREATE	Дата создания.	Да	Нет
DATE_MODIFY	Дата изменения.	Да	Нет
CREATED_BY_ID	Идентификатор создавшего реквизит.	Да	Нет
MODIFY_BY_ID	Идентификатор изменившего реквизит.	Да	Нет
NAME	Название реквизита.	Да	Да
CODE	Символьный код реквизита.	Да	Да
//////////XML_ID	Внешний ключ, используется для операций обмена. Идентификатор объекта внешней информационной базы. Назначение поля может меняться конечным разработчиком.	Да	Да
ACTIVE	Признак активности.	Да	Да
SORT	Сортировка.	Да	Да
////RQ_BANK_NAME	Наименование банка.	Да	Да
//RQ_BANK_ADDR	Адрес банка.	Да	Да
RQ_BANK_ROUTE_NUM	Bank Routing Number.	Да	Да
RQ_BIK	БИК.	Да	Да
RQ_MFO	МФО.	Да	Да
RQ_ACC_NAME	Bank Account Holder Name.	Да	Да
////RQ_ACC_NUM	Bank Account Number.	Да	Да
RQ_IIK	ИИК.	Да	Да
RQ_ACC_CURRENCY	Валюта счёта.	Да	Да
RQ_COR_ACC_NUM	Кор. счёт.	Да	Да
RQ_IBAN	IBAN.	Да	Да
RQ_SWIFT	SWIFT.	Да	Да
RQ_BIC	BIC.	Да	Да
COMMENTS	Комментарий.	Да	Да
ORIGINATOR_ID	Идентификатор внешней информационной базы. Назначение поля может меняться конечным разработчиком.	Да	Да

 *
 * */





//                'Ref_Key' => $value['Ref_Key'],
//                'client_Ref_Key' =>$value['Owner'],
//                'Description' =>$value['Description'],
//                'НомерСчета' => $value['НомерСчета'],
//                'bank_Parent_Key'=> $value['Банк']['Parent_Key'],
//                'bank_Страна_Key' => $value['Банк']['Страна_Key'],
//                'bank_Ref_Key' =>  $value['Банк']['Ref_Key'],
//                'bank_Predefined' => $value['Банк']['Predefined'],
//                'bank_Description' =>  $value['Банк']['Description'],
//                'bank_Code' =>$value['Банк']['Code'],
//                 'bank_ГородМеждународный' =>$value['Банк']['ГородМеждународный'],
//                 'bank_МеждународноеНаименование' =>$value['Банк']['МеждународноеНаименование'],
//                 'bank_IsFolder' =>$value['Банк']['IsFolder'],
//                 'bank_КоррСчет' =>$value['Банк']['КоррСчет'],
//                 'bank_АдресМеждународный' =>$value['Банк']['АдресМеждународный'],
//                 'bank_Телефоны' =>$value['Банк']['Телефоны'],
//                 'bank_DataVersion' =>$value['Банк']['DataVersion'],
//                 'bank_ДеятельностьПрекращена' =>$value['Банк']['ДеятельностьПрекращена'],
//                    'bank_СВИФТБИК' =>$value['Банк']['СВИФТБИК'],
//                    'bank_Адрес' =>$value['Банк']['Адрес'],
//                    'bank_ИНН' =>$value['Банк']['ИНН'],
//                    'Parent@navigationLinkUrl' =>$value['Банк']['Parent@navigationLinkUrl'],
//                    'Страна@navigationLinkUrl' =>$value['Банк']['Страна@navigationLinkUrl']
        }

        return $result;


    }


    public  function FormaInsert($data){
        $result = [
            "auth" => $_REQUEST['AUTH_ID'],
            'fields' =>$data
        ];
        return $result;
    }

    public  function FormatlistUpdate($data)
    {

               $result = [
                    'ID'=>'',
                    'ORIGIN_ID' => $data['fields']['ORIGIN_ID'],
                    'COMPANY_TYPE' => 'CUSTOMER',
                    'TITLE' => $data['fields']['TITLE'],
                    'OPENED' => 'Y',
                    'PHONE' =>  $data['fields']['PHONE']
            ];

        return $result;


    }
    public  function FormatUpdate($id, $data=[])
    {

        $result = [
            "auth" => $_REQUEST['AUTH_ID'],
            'ID'=>$id,
            'fields' => $data
        ];


        return $result;


    }

    public  function Formatlist(){
        $result = ["auth" => $_REQUEST['AUTH_ID'], 'order'=>['ID'=>'ASC'],'select' => ["*"],'filter'=>['!XML_ID'=>false]];
        return $result;
    }



}