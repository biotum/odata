<?php
//if (!class_exists('Cfg')) {
//    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
//}
//if (!class_exists('CurlClient')) {
//    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
//}
//if (!class_exists('ApiClient')) {
//    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
//}
if (!class_exists('PushFormatterInterface.php')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushFormatterInterface.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

class PushCompanyFormatter implements PushFormatterInterface
{
    private $onwerKey;
    /**
     * @param PushDealRequest $clientRequest
     * @return array|mixed
     */
    public  function Format(PushDealRequest $clientRequest)
    {
        $company=$clientRequest->getClient();
        $requisite=$clientRequest->getRequisite();
        $result=[
//"Ref_Key": "d2f119be-831f-11e7-0696-005056891ac8",
"DataVersion"=>"AAAAAAAaisA=",
"DeletionMark"=>false,
"Description"=>$company['TITLE'],
"ИНН"=>$requisite['RQ_INN'],
"ОбособленноеПодразделение"=>false,
"ЮридическоеФизическоеЛицо"=>"ЮридическоеЛицо",
"ГоловнойКонтрагент_Key"=> $company['ORIGIN_ID'],
"КодПоОКПО"=>"",
"КПП"=>$requisite['RQ_KPP'],
"НаименованиеПолное"=>$requisite['RQ_COMPANY_FULL_NAME'],
"ДополнительнаяИнформация"=>"",
"Партнер_Key"=>$company['ORIGINATOR_ID'],
"ЮрФизЛицо"=>"ЮрЛицо",
"НДСПоСтавкам4и2"=>false,
"СтранаРегистрации_Key"=>"00000000-0000-0000-0000-000000000000",
"РегистрационныйНомер"=>$requisite['RQ_OGRN']?$requisite['RQ_OGRN']:$requisite['RQ_OGRNIP'],
"НалоговыйНомер"=>"",
"НаименованиеМеждународное"=>"",
"КонтактнаяИнформация"=>$this->getAddress($clientRequest->getAddressrequisite(),$company['ORIGIN_ID'], $company),
"ДополнительныеРеквизиты"=>[],
"ИсторияКПП"=>[],
"ИсторияНаименований"=>[],
"Predefined"=>false,
"PredefinedDataName"=>"",
'ГоловнойКонтрагент@navigationLinkUrl'=>'Catalog_Контрагенты(guid "'.$company['ORIGIN_ID'].'")/ГоловнойКонтрагент',
'Партнер@navigationLinkUrl'=>'Catalog_Контрагенты(guid "'.$company['ORIGIN_ID'].'")/Партнер'
        ];
        return $result;
    }

    public function getAddress($array,$guid, $company){
        $result=[];

        foreach ($array as $item=>$value) {

            $result[] = [
                'Ref_Key'=>$guid,
                'LineNumber' => $item+1,
                'Тип' => "Адрес",
                'Вид_Key' => "f57765ca-0ccb-11e8-897d-704d7b7030ea",
                'Представление' => $value['POSTAL_CODE'].','.$value['PROVINCE'].','.$value['CITY'].','.$value['REGION'].', дом'.$value['ADDRESS_1'].','.$value['ADDRESS_2'],
                'ЗначенияПолей' => $this->getFormatAddress($value),
                'Страна' =>$value['COUNTRY'],
                'Регион' => '',
                'Город' =>$value['CITY'],
                'АдресЭП' =>  $company['EMAIL'][0]['VALUE'],
                'ДоменноеИмяСервера' => '',
                'НомерТелефона' => $company['PHONE'][0]['VALUE'],
                'НомерТелефонаБезКодов' => '',
                'ВидДляСписка_Key' => "f57765ca-0ccb-11e8-897d-704d7b7030ea",
            ];
        }
        $result[] = [
            'Ref_Key'=>$guid,
            'LineNumber' => count($array)+1,
            'Тип' => "Телефон",
            'Вид_Key' => "02d8c7ae-0ccc-11e8-897d-704d7b7030ea",
            'Представление' => $company['PHONE'][0]['VALUE'],
            'ЗначенияПолей' =>$this->getFormatPhone($company),
            'Страна' =>$value['COUNTRY'],
            'Регион' => '',
            'Город' =>$value['CITY'],
            'АдресЭП' =>"",
            'ДоменноеИмяСервера' => '',
            'НомерТелефона' => $company['PHONE'][0]['VALUE'],
            'НомерТелефонаБезКодов' => $company['PHONE'][0]['VALUE'],
            'ВидДляСписка_Key' => "02d8c7ae-0ccc-11e8-897d-704d7b7030ea",
            // 'Значение' =>""
        ];
        $result[] = [
            'Ref_Key'=>$guid,
            'LineNumber' => count($array)+2,
            'Тип' => "АдресЭлектроннойПочты",
            'Вид_Key' => "02d8c7af-0ccc-11e8-897d-704d7b7030ea",
            'Представление' =>  $company['EMAIL'][0]['VALUE'],
            'ЗначенияПолей' =>$this->getFormatEmail($company),
            'Страна' =>"",
            'Регион' => '',
            'Город' =>"",
            'АдресЭП' =>"",
            'ДоменноеИмяСервера' => '',
            'ВидДляСписка_Key' => "02d8c7af-0ccc-11e8-897d-704d7b7030ea",
            // 'Значение' =>""
        ];

        return $result;
    }

    /**
     * @param $array
     * @return string
     */
    public function getFormatAddress($array){

      return "<КонтактнаяИнформация
       xmlns=\"http://www.v8.1c.ru/ssl/contactinfo\"
        xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" 
        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
        Представление=".$array['POSTAL_CODE'].",".$array['PROVINCE'].",".$array['CITY'].",".$array['REGION'].", дом".$array['ADDRESS_1'].",".$array['ADDRESS_2'].">
        <Комментарий/><Состав xsi:type=\"Адрес\" Страна=".$array['COUNTRY'].">
        <Состав xsi:type=\"АдресРФ\">
        <СубъектРФ>".$array['PROVINCE']."</СубъектРФ>
        <Город>".$array['CITY']."</Город>
        <Улица>".$array['REGION']."</Улица>
        <ДопАдрЭл ТипАдрЭл=\"10100000\" Значение=".$array['POSTAL_CODE']."/>
        <ДопАдрЭл><Номер Тип=\"1010\" Значение=".$array['ADDRESS_1']."/></ДопАдрЭл>
        <ДопАдрЭл><Номер Тип=\"1080\" Значение=".$array['ADDRESS_2']."/></ДопАдрЭл>
        </Состав></Состав>
        </КонтактнаяИнформация>";

    }
    /**
     * @param $phone
     * @return string
     */
    public function getFormatPhone($phone){

        return "<КонтактнаяИнформация 
    xmlns=\"http://www.v8.1c.ru/ssl/contactinfo\"
    xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"
     xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" 
     Представление=".$phone['PHONE'][0]['VALUE'].">
     <Состав xsi:type=\"НомерТелефона\" КодСтраны=\"\" КодГорода=\"\" Номер=".$phone['PHONE'][0]['VALUE']." Добавочный=\"\"/>
     </КонтактнаяИнформация>";

    }
    /**
     * @param $phone
     * @return string
     */
    public function getFormatEmail($email){

        return "<КонтактнаяИнформация 
        xmlns=\"http://www.v8.1c.ru/ssl/contactinfo\"
        xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"
         xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" 
         Представление=".$email['EMAIL'][0]['VALUE'].">
        <Состав xsi:type=\"ЭлектроннаяПочта\" 
        Значение=".$email['EMAIL'][0]['VALUE']."/>
        </КонтактнаяИнформация>";

    }
    public function setOnwerKey($key){
        $this->onwerKey=$key;
    }
    public function getOnwerKey(){
        return $this->onwerKey;
    }
}