<?php
if (!class_exists('Cfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('StoreFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/StoreFormatterInterface.php');
}
if (!class_exists('StoreRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataStore/StoreRequest.php');
}

class CompanyStoreFormatter implements StoreFormatterInterface
{

    public  function Format(StoreRequest $storeRequest)
    {

        foreach ($storeRequest->getStore() as $store) {

                $result[] = [
                    "auth" => $_REQUEST['AUTH_ID'],
                    'fields' => [
                        'XML_ID'=>$store['Ref_Key'],
                        'ORDER_ID'=> $store['Owner_Key'],
                        'NAME'=>$store['Description']
                                        ]
                ];

        }
        return $result;


    }


    public  function FormaInsert($data){
        return false;
    }

    public  function FormatlistUpdate($data)
    {

               $result = [
                    'ID'=>'',
                   'XML_ID'=>$data['fields']['XML_ID'],
                   'ORDER_ID'=>$data['fields']['ORDER_ID'],
                   'NAME'=>$data['fields']['NAME'],
                   'ADDRESS'=>$data['fields']['ADDRESS'],
            ];

        return $result;


    }
    public  function FormatUpdate($data=[])
    {

        return [
            'ID'=> $data['ID'],
            'fields' => [
                'XML_ID'=>$data['XML_ID'],
                'ORDER_ID'=> $data['ORDER_ID'],
                'NAME'=>$data['NAME'],
                'ADDRESS'=>$data['ADDRESS']
           ]
        ];
    }

    public  function Formatlist(){
        $result = ["auth" => $_REQUEST['AUTH_ID'],'order'=>['ID'=>'ASC'], 'select' => ["*"]];
        return $result;
    }

    protected function array_unique_key($array, $key) {
        $tmp = $key_array = array();
        $i = 0;

        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $tmp[$i] = $val;
            }
            $i++;
        }
        return $tmp;
    }

}