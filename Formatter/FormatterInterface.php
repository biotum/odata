<?php

if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
}
if (!class_exists('ClientsRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

interface FormatterInterface
{

    /**
     * @return mixed
     */
    public  function Format(ClientsRequest $clientsRequest);

    /**
     * @return mixed
     */
    public  function FormatlistUpdate($data);
    public  function FormatUpdate ($id, $data);
    public  function Formatlist();
    public  function FormaInsert ($data);



}