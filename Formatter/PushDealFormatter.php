<?php

if (!class_exists('PushFormatterInterface.php')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/PushFormatterInterface.php');
}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

class PushDealFormatter implements PushFormatterInterface
{
    private $onwerKey;
    /**
     * @param PushDealRequest $clientRequest
     * @return array|mixed
     */
    public  function Format(PushDealRequest $clientRequest)
    {

        $result=[
           // 'Ref_Key'=>$clientRequest->getORIGINID(),
            'DataVersion'=>'AAAAAAAbf2E=',
            'DeletionMark'=>false,
            'Description'=>$clientRequest->getTITLE(),
            'ID_Bitrix'=>$clientRequest->getDEALID(),
            'НазваниеСделки'=>$clientRequest->getTITLE(),
            'Валюта_Key'=>'7a595fb0-0ccc-11e8-897d-704d7b7030ea',
            'Сумма'=>$clientRequest->getOPPORTUNITY(),
            'Ответственный'=>$clientRequest->getASSIGNEDBYNAME(),
            'ДатаСоздания'=>$clientRequest->getDATACREATE(),
            'Тип'=> $clientRequest->getTYPE(),
            'Плательщик_Key'=>$clientRequest->getPAYER(),
            'Грузополучатель_Key'=>$clientRequest->getCONSIGNEEID(),
            'Комментарий'=>$clientRequest->getCOMMENTS(),
            'Товар_Key'=>$clientRequest->getPRODUCTID(),
            'Цена'=>$clientRequest->getPRICE(),
            'Количество'=>$clientRequest->getQUANTITY(),
            'ИтогоСНДС'=>$clientRequest->getOPPORTUNITY(),
            'ПунктыРазгрузки'=>$clientRequest->getUPLOADINGPOINTS(),
            'Predefined'=>false,
            'PredefinedDataName'=>'',
            'Валюта@navigationLinkUrl'=>'Catalog_ЗаявкаКлиента(guid "a09d1446-cfd2-11e9-904e-00155d01050d")/Валюта',
            'Плательщик@navigationLinkUrl'=>'Catalog_ЗаявкаКлиента(guid "'.$clientRequest->getPAYER().'")/Плательщик',
            'Грузополучатель@navigationLinkUrl'=>'Catalog_ЗаявкаКлиента(guid "'.$clientRequest->getCONSIGNEEID().'")/Грузополучатель',
            'Товар@navigationLinkUrl'=>'Catalog_ЗаявкаКлиента(guid "'.$clientRequest->getPRODUCTID().'")/Товар'

        ];



        return $result;


    }


    public function setOnwerKey($key){
        $this->onwerKey=$key;
    }
    public function getOnwerKey(){
        return $this->onwerKey;
    }

}