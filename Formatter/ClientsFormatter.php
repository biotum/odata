<?php
if (!class_exists('Cfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('FinishedProductSectionFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FormatterInterface.php');
}
if (!class_exists('ClientsRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

class ClientsFormatter implements FormatterInterface
{

    public  function Format(ClientsRequest $clientRequest)
    {

        foreach ($clientRequest->getClients() as $client) {

                $result[] = [
                    "auth" => $_REQUEST['AUTH_ID'],
                    'fields' => [
                        'ORIGIN_ID' => $client['Ref_Key'],
                        'COMPANY_TYPE' => 'CUSTOMER',
                        'TITLE' => $client['Description'],
                        'OPENED' => 'Y',
                        'PHONE' => $client['КонтактнаяИнформация'][0]['НомерТелефона']
                    ]
                ];
        }

        return $result;


    }


    public  function FormaInsert($data){
        return false;
    }

    public  function FormatlistUpdate($data)
    {

               $result = [
                    'ID'=>'',
                    'ORIGIN_ID' => $data['fields']['ORIGIN_ID'],
                    'COMPANY_TYPE' => 'CUSTOMER',
                    'TITLE' => $data['fields']['TITLE'],
                    'OPENED' => 'Y',
                    'PHONE' =>  $data['fields']['PHONE']
            ];

        return $result;


    }
    public  function FormatUpdate($id, $data=[])
    {

        $result = [
            "auth" => $_REQUEST['AUTH_ID'],
            'ID' => $id,
            'fields' => [
                'TITLE' => $data['TITLE'],
                'COMPANY_TYPE' => 'CUSTOMER',
                'OPENED' => 'Y',
                'PHONE' => [['VALUE' => $data['PHONE'], 'VALUE_TYPE' => 'WORK']]
            ]
        ];


        return $result;


    }

    public  function Formatlist(){
        $result = ["auth" => $_REQUEST['AUTH_ID'], 'order'=>['ID'=>'ASC'],'select' => ["ID","ORIGIN_ID","TITLE"]];
        return $result;
    }



}