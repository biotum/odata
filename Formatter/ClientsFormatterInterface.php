<?php

if (!class_exists('ClientsFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
}
if (!class_exists('ClientsRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceOdataClients/ClientsRequest.php');
}

interface ClientsFormatterInterface
{

    /**
     * @return mixed
     */
    public static function Format(ClientsRequest $finishedProductRequest);

    /**
     * @return mixed
     */
    public static function FormatlistUpdate($data);
    public static function FormatUpdate ($id, $data);
    public static function Formatlist();



}