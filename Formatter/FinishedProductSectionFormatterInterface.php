<?php

if (!class_exists('FinishedProductFormatter')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductSectionFormatter.php');
}
if (!class_exists('FinishedProductSectionRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/FinishedProductSectionRequest.php');
}

interface FinishedProductSectionFormatterInterface
{

    /**
     * @return mixed
     */
    public static function FormatAdd(FinishedProductSectionRequest $finishedProductSectionRequest);

    /**
     * @return mixed
     */
    public static function FormatUpdate($id, $data);


}