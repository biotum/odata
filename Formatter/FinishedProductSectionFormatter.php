<?php
if (!class_exists('Cfg')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Config/cfg.php');
}
if (!class_exists('CurlClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/RestClient/CurlClient.php');
}
if (!class_exists('ApiClient')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/BitrixApi/ApiBitirxClient.php');
}
if (!class_exists('FinishedProductSectionFormatterInterface')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/FinishedProductSectionFormatterInterface.php');
}
if (!class_exists('FinishedProductSectionRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServiceFinishedProduct/FinishedProductSectionRequest.php');
}

class FinishedProductSectionFormatter implements FinishedProductSectionFormatterInterface
{

    public static function FormatAdd(FinishedProductSectionRequest $finishedProductSectionRequest)
    {
        $result = [];
        foreach ($finishedProductSectionRequest->getSections() as $section) {
            $result[] = [
                "auth" => $_REQUEST['AUTH_ID'],
                'fields' => [
                    'CATALOG_ID' => $section['code'],
                    'NAME' => $section['name'],
                    'SECTION_ID' => 0,
                    'XML_ID' => $section['code']
                ]
            ];
        }

        return $result;


    }

    public static function FormatUpdate($id, $data)
    {

        $result = [
            "auth" => $_REQUEST['AUTH_ID'],
            'ID' => $id,
            'fields' => [
                'NAME' => $data,
            ]
        ];


        return $result;


    }
    public static function FormatlistSection(){
        $result = ["auth" => $_REQUEST['AUTH_ID'], 'order'=>['ID'=>'ASC'],'select' => ["ID","XML_ID","NAME"]];
        return $result;
    }
}