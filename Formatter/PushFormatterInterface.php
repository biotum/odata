<?php

//if (!class_exists('ClientsFormatter')) {
//    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/Formatter/ClientsFormatter.php');
//}
if (!class_exists('PushDealRequest')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/marketplace/local/rest_exp_imp_1c/ServicePushOdata/PushDealRequest.php');
}

interface PushFormatterInterface
{

    /**
     * @return mixed
     */
    public  function Format(PushDealRequest $clientsRequest);
    public  function setOnwerKey($key);
    public  function getOnwerKey();


}